import {useDispatch} from "react-redux";
import React from "react";
import Films from "components/Films";
import {Redirect, Route, Switch} from "react-router-dom";
import FilmPage from "components/FilmPage";
import {fetchFilms} from "state/actions/films";
import Header from "./components/Header";
import Login from "./components/Login/Login";
import { Carousel } from 'antd';
const contentStyle = {
    margin: 0,
    height: '160px',
    color: '#fff',
    lineHeight: '160px',
    textAlign: 'center',
    background: '#364d79',
};
const App = () => {
    const onChange = (currentSlide) => {
        console.log(currentSlide);
    };
    
    const dispatch = useDispatch();

    React.useEffect(() => {
        dispatch(fetchFilms());
    }, [dispatch]);
    
const button = document.querySelector('.carousel-button')
 
    
    return (
        <>
            <Header/>
            <Carousel afterChange={onChange}>
                <div className="spinner-border" role="status">
                    <span className="sr-only">Loading...</span>
                </div>
                <div>
                    <button className='carousel-button'> {'<'} </button>
                    <h3 style={contentStyle}>1</h3>
                </div>
                <div>
                    <h3 style={contentStyle}>2</h3>
                </div>
                <div>
                    <h3 style={contentStyle}>3</h3>
                </div>
                <div>
                    <h3 style={contentStyle}>4</h3>
                </div>
                {/*<button> > </button>*/}
            </Carousel>
            <Switch>
                <Redirect exact from='/' to='/films'/>
                <Route exact path='/films' component={Films}>
                </Route>
                <Route path="/films/:filmId" component={FilmPage}/>
                <Route path="/login" component={Login}></Route>
            </Switch>
        </>
    )
}


export default App;
