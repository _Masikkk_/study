import {useFormik} from "formik";
import * as Yup from "yup";

const Schema = Yup.object().shape({
    mail: Yup.string().required().email(),
    password: Yup.string().required().min(6)
})

function FormikForm({onSubmit}) {
    const formik = useFormik({
        initialValues: {
            mail: "",
            password: "",
        },
        onSubmit(values) {
            onSubmit(values);
        },
        validationSchema: Schema,
        validateOnChange: false
    });

    return <form onSubmit={formik.handleSubmit}>
        <input onChange={formik.handleChange} name="mail" type="email" value={formik.values.mail}
               placeholder="example@gmail.com"/>
        <p>{formik.errors.mail}</p>
        <input onChange={formik.handleChange} name="password" type="password"
               value={formik.values.password}/>
        <p>{formik.errors.password}</p>
        <br/>
        <button type="submit">Submit</button>
    </form>
}

export default FormikForm