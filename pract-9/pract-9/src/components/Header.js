import {Link} from "react-router-dom";

function Header() {
    return <ul>
        <Link to="/films">Films</Link>
        <br/>
        <Link to="/login">Login</Link>
    </ul>
}

export default Header