export const FETCH_FILMS_BEGIN = "FETCH_FILMS_BEGIN";
export const FETCH_FILMS_SUCCESS = "FETCH_FILMS_SUCCESS";
export const FETCH_FILMS_FAILURE = "FETCH_FILMS_FAILURE";
export const DELETE_FILM = "DELETE_FILM";

export const fetchFilmsBegin = () => {
	return {type: FETCH_FILMS_BEGIN};
}
export const fetchFilmsSuccess = (data) => {
	return {type: FETCH_FILMS_SUCCESS, payload: data};
}
export const fetchFilmsFailure = (error) => {
	return {type: FETCH_FILMS_FAILURE, payload: error};
}

export const fetchFilms = () => {
	return (dispatch) => {
		dispatch(fetchFilmsBegin());
		fetch("/api/movies", {method: "GET"})
			.then((response) => {
				if (!response.ok) {
					throw new Error("SERVER ERROR");
				} else {
					return response;
				}
			})
			.then((res) => res.json())
			.then((data) => dispatch(fetchFilmsSuccess(data)))
			.catch((err) => dispatch(fetchFilmsFailure(err)))
	}
}

export const deleteFilm = (id) => {
	return {type: DELETE_FILM, payload: +id};
}