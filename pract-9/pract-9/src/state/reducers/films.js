import {DELETE_FILM, FETCH_FILMS_BEGIN, FETCH_FILMS_FAILURE, FETCH_FILMS_SUCCESS} from "state/actions/films";

const initialState = {
	items: [],
	isLoading: false,
	isError: false,
	error: null,
}


export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_FILMS_BEGIN:
			return {...state, isLoading: true};
		case FETCH_FILMS_SUCCESS:
			return {...state, isLoading: false, items: action.payload};
		case FETCH_FILMS_FAILURE:
			return {...state, isLoading: false, isError: true, error: action.payload};
		case DELETE_FILM:
			return {
				...state, items: state.items.filter(item => item.id !== action.payload)
			};
		default:
			return state
	}
}
