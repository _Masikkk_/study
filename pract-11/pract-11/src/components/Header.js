import {Link} from "react-router-dom";
import {useContext} from "react";
import ThemeContext from "context/theme/themeContext"

function Header() {
	const {themes, handleCurrentTheme} = useContext(ThemeContext);

	return <header>
		<ul>
			<Link to="/films">Films</Link>
			<br/>
			<Link to="/login">Login</Link>
		</ul>

		<select onChange={(e) => handleCurrentTheme(e.target.value)}>
			{Object.keys(themes).map(theme => <option key={theme} value={theme}>{theme}</option>)}
		</select>
	</header>
}

export default Header