import {screen, render} from "@testing-library/react";
import Films from "./Films";
import {Provider} from "react-redux";
import thunk from "redux-thunk";
import configureMockStore from "redux-mock-store";

const mockStore = configureMockStore([thunk]);

jest.mock('components/Film', () => (props) => <div data-testid="film">{props.film.name}-{props.film.id}</div>);
jest.mock('components/Loading/Loading', () => () => <div data-testid="Loader">Loading</div>);

describe("Films test", () => {
    it('smoke test', () => {
        const testStore = mockStore({
            films: {
                items: [],
                isLoading: false,
                isError: false,
                error: null,
            }
        })
        render(<Provider store={testStore}><Films/></Provider>)
    });
    it("should component display is loading",()=>{
        const testStore = mockStore({
            films: {
                items: [],
                isLoading: true,
                isError: false,
                error: null,
            }
        })
        render(<Provider store={testStore}><Films/></Provider>)

        const isLoadingElement = screen.getByTestId("Loader");
        expect(isLoadingElement).toBeInTheDocument();
    });
    it("should component display error", ()=>{
        const testStore = mockStore({
            films: {
                items: [],
                isLoading: false,
                isError: true,
                error: new Error("something went wrong"),
            }
        })
        render(<Provider store={testStore}><Films/></Provider>)
        const isErrorElement = screen.getByText("something went wrong");
        expect(isErrorElement).toBeInTheDocument();
    });
    it('should component display not found films',  ()=>{
        const testStore = mockStore({
            films: {
                items: [],
                isLoading: false,
                isError: false,
                error: null,
            }
        })
        render(<Provider store={testStore}><Films/></Provider>)
        const isFilmsNotFoundElement = screen.getByText("No films found");
        expect(isFilmsNotFoundElement).toBeInTheDocument();
    });
    it('should component display 4 films',  ()=>{
        const testStore = mockStore({
            films: {
                items: [
                    {id:1, name:"name-1"},
                    {id:2, name:"name-2"},
                    {id:3, name:"name-3"},
                    {id:4, name:"name-4"}
                ],
                isLoading: false,
                isError: false,
                error: null,
            }
        })
        render(<Provider store={testStore}><Films/></Provider>);
        // const isFilmsFoundElement = screen.getByTestId("films");
        // expect(isFilmsFoundElement.childNodes).toHaveLength(2);

        // const firstFilmElement = screen.getByTestId("film-1");
        // expect(firstFilmElement).toBeInTheDocument();

        // const secondFilmElement = screen.getByTestId("film-2");
        // expect(secondFilmElement).toBeInTheDocument();

        const filmElement = screen.getAllByTestId("film");
        expect(filmElement).toHaveLength(4);
    });
})

