import ControlledForm from "./ControlledForm.js"
import FormikForm from "./FormikForm";

function Login() {
    const handleSubmit = (data) => {
        console.log(data)
    }
    return <>
        <ControlledForm onSubmit={handleSubmit}/>
        <FormikForm onSubmit={handleSubmit}/>
    </>
}

export default Login