import React, {useState, useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {STORE_PERSON} from "state/actions/characters";


function Character(props) {
    const [isLoading, setIsLoading] = useState(true);
    const dispatch = useDispatch();
    const {url} = props;
    const persons = useSelector((state) => state.characters.items);
    useEffect(() => {
        const person = persons[url];
        if (typeof person === "undefined") {
            fetch(url, {
                method: "GET",
            })
                .then((response) => response.json())
                .then((data) => {
                    dispatch({type: STORE_PERSON, payload: {[url]: data}});
                    setIsLoading(false)
                }).catch(e => {
                console.log(e)
            })
        } else {
            setIsLoading(false)
        }
    }, [])
    if (isLoading === true) {
        return <h5>Is Loading</h5>;
    }
    const person = persons[url];
    return <li>{typeof person === "undefined" ? "unknown" : person.name}</li>


}

export default Character;
