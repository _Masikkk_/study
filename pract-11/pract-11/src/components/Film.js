import React from "react";
import {Link} from "react-router-dom";
import {useDispatch} from "react-redux";

import FilmDescription from "components/FilmDescription";
import {deleteFilm} from "state/actions/films";

function Film({film}) {
	const dispatch = useDispatch();
	const [isOpen, setIsOpen] = React.useState(false);

	const toggleOpen = () => {
		setIsOpen((prevIsOpen) => !prevIsOpen);
	};

	const handleDeleteFilm = () => {
		dispatch(deleteFilm(film.id));
	}

	return (
		<li>
			<Link to={`/films/${film.id}`}>{film.name}</Link>
			<button onClick={toggleOpen}>Show more</button>
			<button type="button" onClick={handleDeleteFilm}>X</button>
			{isOpen === true && <FilmDescription film={film}/>}
		</li>
	);
}

export default Film;
