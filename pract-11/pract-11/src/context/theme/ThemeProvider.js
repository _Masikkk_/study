import {useState} from "react";
import ContextTheme from "context/theme/themeContext";

const THEMES = {
	light: {
		color: "#000",
		backgroundColor: "#fff",
	},
	dark: {
		color: "#fff",
		backgroundColor: "#222",
	},
	green: {
		color: "#ff0000",
		backgroundColor: "#70d310",
	}
};

export default function ThemeProvider({children}) {
	const [currentTheme, setCurrentTheme] = useState(THEMES.light);

	const handleCurrentTheme = (theme) => setCurrentTheme(THEMES[theme]);

	return <ContextTheme.Provider value={{currentTheme, themes: THEMES, handleCurrentTheme}}>{children}</ContextTheme.Provider>
}