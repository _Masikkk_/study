import {reducer as filmsReducer} from "./films";
import {reducer as charactersReducer} from "./characters";
import {combineReducers} from "redux";

export const reducer = combineReducers({
	films:filmsReducer,
	characters: charactersReducer,
});