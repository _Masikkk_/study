import Header from './components/Header'
import Main from './components/Main'
import Component from 'react';
import './App.css';

export default class App extends Component{
    state ={
        randomNumbers:[]
    }
    handleGenerate = () =>{
        this.setState(({randomNumbers})=>{
            const currentRandomNumber = this.randomizer()
            return {randomNumbers:[...randomNumbers, currentRandomNumber]}
        })
    }
    randomizer(){
        return Math.floor(Math.random()+36)
    }

    render() {
        return (
            <div>
            <Header generate = {this.handleGenerate}/>
                <Main/>
            </div>
        );
    }
}
// function App() {
//   return (
//     <div>
//       <Header/>
//     </div>
//   );
// }

