import React from "react";
import logo from './logo.svg';
import './App.css';
// import Fcomponent from "./Fcomponent";
import Ccomponent from "./Ccomponent";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        {/*<Fcomponent name='Alex'/>*/}
        <img src={logo} className="App-logo" alt="logo" />
        <Ccomponent />
      </header>
    </div>
  );
}

export default App;
