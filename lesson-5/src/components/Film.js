import React from "react";
import FilmDescription from "./FilmDescription";
import {Link} from "react-router-dom";

function Film({ film, onDelete }) {
  const [isOpen, setIsOpen] = React.useState(false);

  const toggleOpen = () => {
    setIsOpen((prevIsOpen) => !prevIsOpen);
  };



  return (
    <li>
      {film.name} <Link to={`films/${film.id}`}><button onClick={toggleOpen}>More</button></Link>
      {isOpen === true && <FilmDescription film={film} />}
    </li>
  );
}

export default Film;
