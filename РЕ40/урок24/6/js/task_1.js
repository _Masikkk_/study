// Дана таблица с юзерами с двумя колонками: имя и фамилия.
// Под таблицей сделайте форму, с помощью которой можно будет добавить нового юзера в таблицу.
// Сделайте так, чтобы при клике на любую ячейку появлялся prompt, с помощью которого можно изменить
// текст ячейки. Задачу решите с помощью делегирования (то есть событие должно быть навешано на table).


let name =null;
let surname = null;
const form = document.createElement('form')

const inputName = document.createElement('input')
inputName.setAttribute('id', 'name')

const inputSurname = document.createElement('input')
inputSurname.setAttribute('id', 'surname')

const saveButton = document.createElement('button')
saveButton.innerText = 'Save'

const table = document.querySelector('table')

form.append(inputName,inputSurname,saveButton)
document.body.append(form)

const nameInput = document.body.querySelector('#name')
const surnameInput = document.body.querySelector('#surname')

saveButton.addEventListener('click', event => {
    event.preventDefault()
    const name = nameInput.value;
    const surname = surnameInput.value;

    const row = document.createElement('tr')
    const nameCell = document.createElement('td')
    const surnameCell = document.createElement('td')

    nameCell.innerText = name;
    nameCell.classList.add('cell')
    row.append(nameCell)
    surnameCell.innerText = surname;
    surnameCell.classList.add('cell')
    row.append(surnameCell)
    table.append(row)

    nameInput.value = '';
    surnameInput.value = '';
})

