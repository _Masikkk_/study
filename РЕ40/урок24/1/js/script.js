// По клику на картинку - создавать дубль этой картинки, в конце списка картинок.
// Использовать делегирование событий


document.addEventListener('click', event => {
    let targetImg = event.target;
    if(targetImg.tagName === `IMG`) {
    const result = targetImg.cloneNode()
        // document.body.append(result)
    const images = document.querySelectorAll('img')
    images[images.length - 1].after(result)
    }
    })