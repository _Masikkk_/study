/**
 * Улучшить существующий код:
 * - Доавить каждому элементу класс "item" и id, в котором будет указан его порядковый номер;
 * - Добавить кнопку удаления к каждому элементу списка. задать кнопке текст: "Х". Присвоить ей класс 'deleteButton';
 * - По клику на кнопку соответствующий элемент должен удаляться;
 * - Использовать делегирование событий;
 *
 */

let input = document.querySelector('input');
let list = document.querySelector('.tasks-list');
let button = document.querySelector('#clear');
function createToDoItem(event) {
    const inputValue = input.value.trim();

    if (event.key === 'Enter' && inputValue.length) {
        let listElement = document.createElement('li');

        listElement.classList.add('item')
        listElement.setAttribute('id',`${list.childElementCount + 1}`)

        const deleteButton = document.createElement('button')
        deleteButton.innerText = 'X'
        deleteButton.classList.add('deleteButton')

        listElement.innerText = inputValue;
        input.value = '';

        list.append(listElement);
        listElement.append(deleteButton)
    }
}

const clearAll = () => {
   const answer = confirm("Are you sure?")
    if (answer){
        list.innerHTML = "";
    }
}

const onKeyDown = event => {
    if (list.childElementCount > 0 && event.code === 'KeyD' && event.ctrlKey) {
        event.preventDefault()
        list.lastChild.remove()

    }
    if(event.key === "Backspace" && event.altKey && event.shiftKey){
        clearAll();
    }
}

button.addEventListener('click', clearAll)
input.addEventListener('keydown', createToDoItem);
document.addEventListener('keydown', onKeyDown);

list.addEventListener('click', event => {
    if (event.target.tagName === 'BUTTON' && event.target.classList.contains('deleteButton')){
        event.target.parentNode.remove()
    }
})