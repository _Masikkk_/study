// Даны инпуты. Сделайте так, чтобы все инпуты по потери фокуса проверяли свое содержимое
// на правильное количество символов. Сколько символов должно быть в инпуте,
// указывается в атрибуте data-length.
// Если вбито правильное количество, то граница инпута становится зеленой,
// если неправильное - красной.

const inputs = document.querySelectorAll('input')

inputs.forEach(input => {
    input.addEventListener('blur', event => {
        let resultLength = event.target.dataset.length;
        if(event.target.value.length > resultLength){
            event.target.style.border = `1px solid red`
        } else {
            event.target.style.border = `1px solid red`

        }
    })
})