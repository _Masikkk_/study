// Есть массив брендов автомобилей ["bMw", "Audi", "teSLa", "toYOTA"].
// Вам нужно получить новый массив, объектов типа
// {
//     type: 'car'
//     brand: ${элемент массива}
// }
// Для всех машин добавить свойство isElectric, для теслы оно будет true, для остальных false;
// Для того чтоб проверять что машина электрическая создать отдельную функцию что будет хранить
// в себе массив брендов электрических машин и будет проверять входящий элемент - на то,
// если он в массиве.
// Создать новый массив, в котором хранятся все машины, что не являются электрокарами.
// Вывести его в модальное окно пользователю.
// let array = ["bMw", "Audi", "teSLa", "toYOTA"];
// const checkIsElectric = name => {
//  let electron = ['TESLA'];
//     return electron.includes(name.toUpperCase());
// }
// const createArray = () => {
//  const newArray = array.map(item => (
//        {
//            type: "car",
//            brand: item.toUpperCase(),
//            isElectric: checkIsElectric(item)
//        }))
//     return newArray.filter(car => !car.isElectric)
// }
//
// console.log(createArray(array))

// Описание задачи: Напишите функцию, которая сравнивает два массива и возвращает true,
// если они идентичны.
// Ожидаемый результат: ([1, 2, 3], [1, 2, 3]) => true
// const arr1 = [1, 2, 3, 4];
// const arr2 = [1, 2, 3, 4];
// const arr3 = [1, 2, 3, 5];
// const arr4 = [1, 2, 3, 4, 5];
//
// const isEqual = (array1, array2) => {
//     let arrayIsEqual = false;
//     if (array1.length !== array2.length){
//         return false
//     }
//     const booleanArray = array1.map((item, index) => {
//         return item === array2[index];
//     })
//     return !booleanArray.includes(false);
// }
//
// console.log(isEqual(arr1, arr2)); // true
// console.log(isEqual(arr1, arr3)); // false
// console.log(isEqual(arr1, arr4)); // false

// const result = [];
// const flatten = array => {
//     array.forEach(item => {
//         if(Array.isArray(item)){
//             flatten(item)
//         }else {
//             result.push(item)
//         }
//     })
//     return result
// }
//
// console.log(flatten([1, 2, [3,4], [5,[6,7]]]))
// console.log(flatten([1, 2, [3,4], [5,[6,7,[8,9],[10,11,[12,13,[14]]]]]]))


// Дан список пользователей с родственными связями. Задачи:
// - Вывести два плоских списка всех юзеров и их родителей, бабушек или дедушек на одном уровне.
// - Первый список должен содержать совершеннолетних пользователей, второй - тех, кому еще нет 18
// - отдельно вывести имя самого старшего из пользователей
// const appUsers = [
//     {
//         id: 1,
//         name: 'Ivan Kulakov',
//         age: 6,
//         parent: {
//             id: 2,
//             name: 'Ann Kulakova',
//             age: 25,
//             parent: {
//                 id: 3,
//                 name: 'Grandfather',
//                 age: 68,
//                 parent: null,
//             }
//         }
//     },
//     {
//         id: 4,
//         name: 'Ivan Avensov',
//         age: 17,
//         parent: {
//             id: 5,
//             name: 'Ann Avensova',
//             age: 58,
//             parent: null}
//     },
//     {
//         id: 6,
//         name: 'Ivan Avensov',
//         age: 67,
//         parent: null,
//     }
// ]
//
// const getUserlist = users => {
//     const flatUsers = [];
//     const getUsers = (users) => {
//         users.forEach(user => {
//             if (!user.parent){
//                 flatUsers.push(user);
//             } else {
//                 flatUsers.push(user);
//                 getUsers([user.parent]);
//             }
//         })
//
//     }
//     getUsers(users)
//     return flatUsers
// }
// console.log(getUserlist(appUsers))

// function pow(x,n) {
//     if (n === 1){
//         return x;
//     } else {
//         return x * pow(x, n - 1)
//     }
// }
// console.log(pow(2,5))

 // Создать массив объектов students (в количестве 7 шт).
// У каждого студента должно быть имя, фамилия и направление обучения - Full-stack
// или Front-end. У каждого студента должен быть метод, sayHi, который возвращает строку
// `Привет, я ${имя}, студент Dan, направление ${направление}`.
// Перебрать каждый объект и вызвать у каждого объекта метод sayHi;
//
// const students = [
//     {
//         id:1,
//         name: 'Ann',
//         lastName: 'Petrova',
//         major: 'Front-End',
//         sayHi() {
//             console.log(`Привет, я ${this.name}, студент Dan, направление ${this.major}`);
//         }
//     },
//     {
//         id:2,
//         name:'Anton',
//         lastName:'Rud',
//         major: 'Full-Stack',
//         sayHi() {
//             console.log(`Привет, я ${this.name}, студент Dan, направление ${this.major}`);
//         }
//     },
//     {
//         id:3,
//         name: 'Kate',
//         lastName: 'Ikonenko',
//         major: 'Front-End',
//         sayHi() {
//             console.log(`Привет, я ${this.name}, студент Dan, направление ${this.major}`);
//         }
//     },
//     {
//         id:4,
//         name:'Vlad',
//         lastName:'Ivanov',
//         major: 'Full-Stack',
//         sayHi() {
//             console.log(`Привет, я ${this.name}, студент Dan, направление ${this.major}`);
//         }
//     },
//     {
//         id:5,
//         name: 'Alina',
//         lastName: 'Yuschenko',
//         major: 'Front-End',
//         sayHi() {
//             console.log(`Привет, я ${this.name}, студент Dan, направление ${this.major}`);
//         }
//     },
//     {
//         id:6,
//         name:'Sasha',
//         lastName:'Volkov',
//         major: 'Full-Stack',
//         sayHi() {
//             console.log(`Привет, я ${this.name}, студент Dan, направление ${this.major}`);
//         }
//     },
//     {
//         id:7,
//         name: 'Lora',
//         lastName: 'Pechkina',
//         major: 'Full-Stack',
//         sayHi() {
//             console.log(`Привет, я ${this.name}, студент Dan, направление ${this.major}`);
//         }
//     },
// ]
// students.forEach(student => {
//     student.sayHi = function () {
//         console.log(`Привет, я ${this.name}, студент Dan, направление ${this.major}`);
//     };
// })
//
// students.forEach(student => {
//     student.sayHi();
// })

// Дан список игроков. Нужно отсортировать массив по убыванию, и добавить каждому игроку поле place,
//   в котором будет указано
//   его место в списке игроков - то есть Иван должен оказаться на первом месте, Василий - на последнем


// const users = [
//     { name: 'Victor', score: 20 },
//     { name: 'Mario', score: 10 },
//     { name: 'Tatiana', score: 30 },
//     { name: 'Vasiliy', score: 0 },
//     { name: 'Ivan', score: 100 },
// ];
// users.sort((a,b) => b.score - a.score);
// const usersPlace = users.map((item, index) => {
//     return {...item, place: index + 1}
// });
// console.log(usersPlace)
// console.log(users)
