// Задание

// Программа будет выводить то или иное сообщение на страницу в зависимости от возраста пользователя.
// Сообщение представляет из себя html-элемент div. В нем будет сообщение (далее alert-message) и тип
// (тип алерта - это тот или иной класс для div).

// Логика:
// Спросить у пользователя его имя и возраст.
// Если число (также проверить, что там число) меньше 18, вывести на страницу
// сообщение, где alert-message - Sorry, you are not be able to visit us. Тип - 'alert-danger'
// Если больше 18 - alert-message 'Hi, ${name}.', тип 'alert-success';

let name = '';
let age = '';
do{
    name = prompt('Enter your name')
}while(!name)
do{
    age = +prompt('Enter your age')
}while(!age || Number.isNaN(age) || !Number.isInteger(age))

let div = document.createElement('div');
if (age < 18 ){
    div.innerText = 'Sorry, you are not be able to visit us.'
    div.classList.add('alert-danger')
} else {
    div.innerText = `Hi, ${name}.`
    div.classList.add('alert-success')
}
document.body.prepend(div)
