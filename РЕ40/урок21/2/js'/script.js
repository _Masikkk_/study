/**
 * Задание 1.
 *
 * Получить элемент с классом .remove.
 * Удалить его из разметки.
 *
 * Получить элемент с классом .bigger.
 * Заменить ему CSS-класс .bigger на CSS-класс .active.
 *
 * Условия:
 * - Вторую часть задания решить в двух вариантах: в одну строку и в две строки.
 */

/* Удаление */
// document.querySelector('.remove').remove();

/* Изменение: способ 1 */
// document.querySelector('.bigger').classList.replace('bigger', 'active');

/* Изменение: способ 2 */
// document.querySelector('.bigger').classList.remove('bigger');
// document.querySelector('.bigger').classList.add('active');


// document.querySelector('.remove').remove();
// const collection = document.getElementsByClassName('remove');
// collection[0].remove()

// const bigger = document.querySelector('.bigger');
// bigger.classList.replace('bigger','active')
// bigger.classList.remove('bigger')
// bigger.classList.add('active')




