/**
 * Задание 2.
 *
 * На экране указан список товаров с указанием названия и количества на складе.
 *
 * Найти товары, которые закончились и:
 * - Изменить 0 на «закончился»;
 * - Изменить цвет текста на красный;
 * - Изменить жирность текста на 600.
 *
 * Требования:
 * - Цвет элемента изменить посредством модификации атрибута style.
 */

// const elements = document.getElementsByTagName('li');
// for (let listItem of elements) {
//     let string = ': 0'
//     if (listItem.innerText.includes(string)){
//         listItem.innerText = listItem.innerText.replace(string, ': закончился')
//         listItem.style.color = 'red';
//         listItem.style.fontWeight = '700';
//     }
// }