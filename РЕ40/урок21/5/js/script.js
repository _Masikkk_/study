/**
 * Задание 1.
 *
 * Написать скрипт, который создаст квадрат произвольного размера.
 *
 * Размер квадрата в пикселях получить интерактивно посредством диалогового окна prompt.
 *
 * Если пользователь ввёл размер квадрата в некорректном формате —
 * запрашивать данные повторно до тех пор, пока данные не будут введены корректно.
 *
 * Все стили для квадрата задать через JavaScript посредством одной строки кода.
 *
 * Тип элемента, описывающего квадрат — div.
 * Задать ново-созданному элементу CSS-класс .square.
 *
 * Квадрат в виде стилизированного элемента div необходимо
 * сделать первым и единственным потомком body документа.
 */

let sizeSquare = null;
do{
    sizeSquare = +prompt('Enter size of square')
}while (!Number.isInteger(sizeSquare) || !sizeSquare)
// console.log(sizeSquare)
let div = document.createElement('div')
div.style = `width: ${sizeSquare}px; height: ${sizeSquare}px; background: red; border: 5px solid orange`;

div.classList.add('square')
document.body.prepend(div)