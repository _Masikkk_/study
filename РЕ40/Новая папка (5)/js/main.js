"use strict";
//Navbar scrolling to needed section by click
$('.nav-item').click(function () {
    let elementClick = $(this).attr("href")
    let destination = $(elementClick).offset().top;
    $('html, body').animate({
        scrollTop: destination
    }, 1000, 'swing');
});
//Switching tabs in section "Our services"
const navBar = document.querySelector('.main-services');
let prevTab = document.querySelector('.active-tab');
const paragraphList = document.querySelector('.main-services-content')

function opener(tab) {
    for (let i = 0; i < paragraphList.children.length; i++) {
        let paragraphListIndex = paragraphList.children[i];
        if (paragraphListIndex.innerText.includes(tab.innerText)) {
            paragraphListIndex.style.display = 'flex'
        }
    }
}

function closer(tab) {
    for (let i = 0; i < paragraphList.children.length; i++) {
        let paragraphListIndex = paragraphList.children[i];
        if (paragraphListIndex.innerText.includes(tab.innerText)) {
            paragraphListIndex.style.display = 'none'
        }
    }
}

const activeTab = (event) => {
    if (prevTab) {
        prevTab.classList.remove('active-tab')
        prevTab.classList.remove('services-tab-default')
        closer(prevTab)
    }
    event.target.classList.add('active-tab');
    opener(event.target)
    prevTab = event.target
}
navBar.addEventListener('click', activeTab);
//Uploading photos in section "Our Amazing work"
const workLoadBtn = document.querySelector('.main-work-load')
const mainWorkHeight = document.querySelector('.main-work')
const newImg = document.querySelectorAll('.work-photo')
const loadingIcon = document.querySelector('#fountainG')

function workAddPhotos() {
    loadingIcon.style.visibility = 'visible';
    setTimeout(uploadingPhotos, 2000)
}

function uploadingPhotos() {
    let activePhotos = document.querySelectorAll('.active-photo')
    if (activePhotos.length === 12) {
        for (let i = 12; i <= 23; i++) {
            newImg[i].classList.add('active-photo')
        }
        mainWorkHeight.style.height = `1842px`;
        loadingIcon.style.visibility = 'hidden';
    } else if (activePhotos.length === 24) {
        for (let i = 24; i <= 35; i++) {
            newImg[i].classList.add('active-photo')
        }
        mainWorkHeight.style.height = `2472px`;
        workLoadBtn.style.visibility = "hidden"
        loadingIcon.style.visibility = 'hidden';
    }
}

workLoadBtn.addEventListener('click', workAddPhotos)
//Filtering photo by style
const workPhotos = document.querySelector('.main-work-nav')
const workItem = document.querySelectorAll('.main-work-nav-item')
const showPhotos = (event) => {
    if (event.target !== workPhotos) {
        workItem[0].style.border = "2px solid #EDEFEF"
        workItem[1].style.border = "2px solid #EDEFEF"
        workItem[2].style.border = "2px solid #EDEFEF"
        workItem[3].style.border = "2px solid #EDEFEF"
        workItem[4].style.border = "2px solid #EDEFEF"
        event.target.style.border = "2px solid #18CFAB"
    }
    let activePhotos = document.querySelectorAll('.active-photo')
    if (event.target.innerText === 'Graphic Design') {
        for (let i = 0; i <= activePhotos.length; i++) {
            activePhotos[i].style.display = 'inline-block';
            mainWorkHeight.style.height = `1212px`;
            workLoadBtn.style.visibility = "hidden"
            if (activePhotos[i].getAttribute('photo') !== 'graphic-design') {
                activePhotos[i].style.display = 'none';
            }
        }
    } else if (event.target.innerText === 'Web Design') {
        for (let i = 0; i <= activePhotos.length; i++) {
            activePhotos[i].style.display = 'inline-block';
            mainWorkHeight.style.height = `1212px`;
            workLoadBtn.style.visibility = "hidden"
            if (activePhotos[i].getAttribute('photo') !== 'web-design') {
                activePhotos[i].style.display = 'none';
            }
        }

    } else if (event.target.innerText === 'Landing Pages') {
        for (let i = 0; i <= activePhotos.length; i++) {
            activePhotos[i].style.display = 'inline-block';
            mainWorkHeight.style.height = `1212px`;
            workLoadBtn.style.visibility = "hidden"
            if (activePhotos[i].getAttribute('photo') !== 'landing-pages') {
                activePhotos[i].style.display = 'none';
            }
        }

    } else if (event.target.innerText === 'Wordpress') {
        for (let i = 0; i <= activePhotos.length; i++) {
            activePhotos[i].style.display = 'inline-block';
            mainWorkHeight.style.height = `1212px`;
            workLoadBtn.style.visibility = "hidden"
            if (activePhotos[i].getAttribute('photo') !== 'wordpress') {
                activePhotos[i].style.display = 'none';
            }
        }

    } else if (event.target.innerText === 'All' && activePhotos.length === 12) {
        for (let i = 0; i <= activePhotos.length; i++) {
            mainWorkHeight.style.height = `1212px`;
            workLoadBtn.style.visibility = "visible"
            activePhotos[i].style.display = 'inline-block';

        }
    } else if (event.target.innerText === 'All' && activePhotos.length === 24) {
        for (let i = 0; i <= activePhotos.length; i++) {
            mainWorkHeight.style.height = `1842px`;
            workLoadBtn.style.visibility = "visible"
            activePhotos[i].style.display = 'inline-block';

        }
    } else if (event.target.innerText === 'All' && document.querySelectorAll('.active-photo').length === 36) {
        for (let i = 0; i <= activePhotos.length; i++) {
            workLoadBtn.style.visibility = "hidden"
            mainWorkHeight.style.height = `2472px`;
            activePhotos[i].style.display = 'inline-block';
        }
    }

}
workPhotos.addEventListener('click', showPhotos)
//Changing the person who left the review and changing the review itself
const mainReviewerList = document.querySelector('.main-reviewer-list')
const milaCunnis = document.querySelector('#mila-cunnis')
const georgeWashington = document.querySelector('#george-washington')
const hasanAli = document.querySelector('#hasan-ali')
const lolaMay = document.querySelector('#lola-may')
const mila = document.querySelector('#mila')
const george = document.querySelector('#george')
const hasan = document.querySelector('#hasan')
const lola = document.querySelector('#lola')
const nextReviewerBtn = document.querySelector('.main-reviewer-next')
const previousReviewerBtn = document.querySelector('.main-reviewer-previous')
mainReviewerList.addEventListener('click', (event) => {
    if (event.target.id.includes('mila')) {
        milaCunnis.classList.add('active-review')
        georgeWashington.classList.remove('active-review')
        hasanAli.classList.remove('active-review')
        lolaMay.classList.remove('active-review')
        mila.classList.add('active-reviewer')
        george.classList.remove('active-reviewer')
        hasan.classList.remove('active-reviewer')
        lola.classList.remove('active-reviewer')
    } else if (event.target.id.includes('george')) {
        milaCunnis.classList.remove('active-review')
        georgeWashington.classList.add('active-review')
        hasanAli.classList.remove('active-review')
        lolaMay.classList.remove('active-review')
        mila.classList.remove('active-reviewer')
        george.classList.add('active-reviewer')
        hasan.classList.remove('active-reviewer')
        lola.classList.remove('active-reviewer')
    } else if (event.target.id.includes('hasan')) {
        milaCunnis.classList.remove('active-review')
        georgeWashington.classList.remove('active-review')
        hasanAli.classList.add('active-review')
        lolaMay.classList.remove('active-review')
        mila.classList.remove('active-reviewer')
        george.classList.remove('active-reviewer')
        hasan.classList.add('active-reviewer')
        lola.classList.remove('active-reviewer')
    } else if (event.target.id.includes('lola')) {
        milaCunnis.classList.remove('active-review')
        georgeWashington.classList.remove('active-review')
        hasanAli.classList.remove('active-review')
        lolaMay.classList.add('active-review')
        mila.classList.remove('active-reviewer')
        george.classList.remove('active-reviewer')
        hasan.classList.remove('active-reviewer')
        lola.classList.add('active-reviewer')
    }
})
nextReviewerBtn.addEventListener('click', () => {
    if (document.querySelector('.active-review') === document.querySelector('#hasan-ali')) {
        hasanAli.classList.remove('active-review')
        hasan.classList.remove('active-reviewer')
        lolaMay.classList.add('active-review')
        lola.classList.add('active-reviewer')
    } else if (document.querySelector('.active-review') === document.querySelector('#lola-may')) {
        lolaMay.classList.remove('active-review')
        lola.classList.remove('active-reviewer')
        milaCunnis.classList.add('active-review')
        mila.classList.add('active-reviewer')
    } else if (document.querySelector('.active-review') === document.querySelector('#mila-cunnis')) {
        milaCunnis.classList.remove('active-review')
        mila.classList.remove('active-reviewer')
        georgeWashington.classList.add('active-review')
        george.classList.add('active-reviewer')
    } else if (document.querySelector('.active-review') === document.querySelector('#george-washington')) {
        georgeWashington.classList.remove('active-review')
        george.classList.remove('active-reviewer')
        hasanAli.classList.add('active-review')
        hasan.classList.add('active-reviewer')
    }
})
previousReviewerBtn.addEventListener('click', () => {
    if (document.querySelector('.active-review') === document.querySelector('#hasan-ali')) {
        hasanAli.classList.remove('active-review')
        hasan.classList.remove('active-reviewer')
        georgeWashington.classList.add('active-review')
        george.classList.add('active-reviewer')
    } else if (document.querySelector('.active-review') === document.querySelector('#george-washington')) {
        georgeWashington.classList.remove('active-review')
        george.classList.remove('active-reviewer')
        milaCunnis.classList.add('active-review')
        mila.classList.add('active-reviewer')
    } else if (document.querySelector('.active-review') === document.querySelector('#mila-cunnis')) {
        milaCunnis.classList.remove('active-review')
        mila.classList.remove('active-reviewer')
        lolaMay.classList.add('active-review')
        lola.classList.add('active-reviewer')
    } else if (document.querySelector('.active-review') === document.querySelector('#lola-may')) {
        lolaMay.classList.remove('active-review')
        lola.classList.remove('active-reviewer')
        hasanAli.classList.add('active-review')
        hasan.classList.add('active-reviewer')
    }
})
//Uploading more photos in "Our gallery" - сделал на 1 добавление
const galleryLoadBtn = document.querySelector('.gallery-load')
const newImgGallery = document.querySelectorAll('.gallery-image')
const loadingGalleryIcon = document.querySelector('#fountainB')

function galleryAddPhotos() {
    loadingGalleryIcon.style.visibility = 'visible';
    setTimeout(uploadingImages, 2000)
}

function uploadingImages() {
    let activeImages = document.querySelectorAll('.active-image')
    if (activeImages.length === 18) {
        for (let i = 0; i <= 35; i++) {
            newImgGallery[i].classList.add('active-image')
        }
        loadingGalleryIcon.style.visibility = 'hidden';
        galleryLoadBtn.style.visibility = 'hidden';
    }
}
galleryLoadBtn.addEventListener('click', galleryAddPhotos)

