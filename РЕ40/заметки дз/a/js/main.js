// let olList = document.createElement("ol")
// let listItem = document.createElement('li')
// document.body.append(olList)
// olList.append(listItem)
//
// const createList = (arr, parent = document.body) => {
// arr.map(element => {
// `<li>${element}</li>`
// console.log(arr)
// let ulList = document.createElement("ol")
// if (Array.isArray(element)) {
//     parent.append(ulList)
//     createList(element, ulList)
// } else {
//     ulList.innerHTML = `12`
//     parent.append(ulList)
// }
// ulList.innerHTML= arr.map(element => `<li>${element}</li>`)
// ulList.insertAdjacentHTML("afterend", ``);
// }).join()
//
// }
// createList(["hello", "world", ["Kiev", "Kharkiv"], "Odessa", "Lviv"])
const createList = (arr, parent = document.body) => {

    const innerContent = arr.map(el => {
        if (Array.isArray(el)) {
            const li = document.createElement('li');
            createList(el, li)
            return li.outerHTML
        } else {

            return `<li>${el}</li>`;
        }
    }).join('');
    parent.innerHTML += `<ol>${innerContent}</ol>`
    console.log(setTimeout(parent.innerHTML = '', 1000));
};

createList(['hello', 'world', ['Inner 1', 'Inner 2'], 'outer']);