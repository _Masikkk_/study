/**
 * При натисканні на кнопку Validate відображати
 * VALID зеленим кольром, якщо значення проходить валідацію
 * INVALID червоним кольором, якщо значення не проходить валідацію
 *
 * Правила валідації значення:
 * - значення не пусте
 *
 * ADVANCED
 * Правила валідації значення:
 * - повинно містити щонайменше 5 символів
 * - не повинно містити пробілів
 * - повинно починатися з літери (потрібно використати регулярні вирази)
 *
 */



const buttonClick = document.getElementById('validate-btn')
let valid = document.createElement('div')
const handler = function (e){
    const inputField = document.getElementById('input')
    let content = inputField.value;
    if(content && content.length >= 5 && !content.includes(' ') && content[0].match(/^[a-z A-Z]/)){
        valid.innerText = 'VALID';
        valid.style.color = 'green'
}else {
        valid.innerText = 'INVALID';
        valid.style.color = 'red'
    }
    document.body.append(valid)
}
buttonClick.addEventListener('click',handler)