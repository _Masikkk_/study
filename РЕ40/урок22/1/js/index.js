const button = document.createElement('button')
button.addEventListener('click', ()=> {
    alert('Welcome!')
})
button.addEventListener('mouseover', () => {
    alert('If you click on button, you come in the system')
},{once: true})
button.innerText = 'Come in'
document.body.append(button)