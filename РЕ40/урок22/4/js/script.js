/**
 * Початкове значення лічильника 0
 * При натисканні на + збільшувати лічильник на 1
 * При натисканні на - зменшувати лічильник на 1
 *
 * ADVANCED: не давати можливості задавати лічильник менше 0
 *
 */
let counter = document.getElementById('counter')
const decrementButton = document.getElementById('decrement-btn')
const incrementButton = document.getElementById('increment-btn')
let number = 0;

decrementButton.addEventListener('click',()=> {
if (number > 0){
    number--
    counter.innerText = `Counter: ${number}`
}
})
incrementButton.addEventListener('click', ()=>{
    number++
    counter.innerText = `Counter: ${number}`
})
