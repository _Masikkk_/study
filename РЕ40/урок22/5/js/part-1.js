/**
 * Задание 3.
 *
 * Создать элемент h1 с текстом «Добро пожаловать!».
 *
 * Под элементом h1 добавить элемент button c текстом «Раскрасить».
 *
 * При клике по кнопке менять цвет каждой буквы элемента h1 на случайный.
 */

/* Дано */
const PHRASE = 'Добро пожаловать!';

function getRandomColor() {
  const r = Math.floor(Math.random() * 255);
  const g = Math.floor(Math.random() * 255);
  const b = Math.floor(Math.random() * 255);

  return `rgb(${r}, ${g}, ${b})`;
}

/* Решение */

const h1 = document.createElement('h1')
h1.innerText = PHRASE;
document.body.append(h1)
const buttonClick = document.createElement('button')
buttonClick.innerText = 'color letter'
document.body.append(buttonClick)
// buttonClick.addEventListener('click', () => {
//       h1.innerText = '';
//   for(let i = 0; i < PHRASE.length; i++){
// let letter = document.createElement('span')
//     letter.innerText = PHRASE[i]
//     letter.style.color = getRandomColor()
//     setTimeout(() => h1.append(letter), i * 200)
//     // h1.append(letter)
//     }
//       for (let char of PHRASE) {
//         let span = document.createElement('span')
//         span.innerText = char;
//         span.style.color = getRandomColor();
//         h1.append(span)
//       }
//     }
// )
const handler = () => {
  h1.innerText = '';
  for (let i = 0; i < PHRASE.length; i++) {
    let span = document.createElement('span')
    span.innerText = PHRASE[i]
    span.style.color = getRandomColor()
    h1.append(span);
  }}

buttonClick.addEventListener('click', handler)
document.body.addEventListener('mousemove', handler)