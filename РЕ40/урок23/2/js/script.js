/**
 * При натисканні на enter в полі вводу
 * додавати його значення, якщо воно не пусте,
 * до списку задач та очищувати поле вводу
 *
 * При натисканні Ctrl + D на сторінці видаляти
 * останню додану задачу
 *
 * Додати можливість очищувати весь список
 * Запустити очищення можна двома способами:
 * - при кліці на кнопку Clear all
 * - при натисканні на Alt + Shift + Backspace
 *
 * При очищенні необхідно запитувати у користувача підтвердження
 * (показувати модальне вікно з вибором Ok / Cancel)
 * Якщо користувач підвердить видалення, то очищувати список,
 * інакше нічого не робити зі списком
 *
 */

let input = document.querySelector('input')
let ul = document.querySelector('.tasks-list')
let button = document.getElementById('clear')
const createToDoElement = e => {
    const inputValue = input.value.trim()
        if (e.key === 'Enter' && inputValue.length) {
            let listElem = document.createElement('li')
            listElem.innerText = input.value;
            input.value = '';
            ul.append(listElem)
        }
}
const deleteElement = e => {
    if (ul.childElementCount > 0 && e.code === 'KeyQ' && e.ctrlKey) {
        e.preventDefault()
        ul.lastChild.remove()
    }
    if(e.altKey && e.shiftKey && e.key === 'Backspace'){
        clearAll()
    }
}
const clearAll = () => {
    const result = confirm('Are you sure?')
    if(result){
    ul.innerHTML = '';
    }
}

button.addEventListener('click', clearAll)
input.addEventListener('keydown', createToDoElement)
document.addEventListener('keydown', deleteElement)

