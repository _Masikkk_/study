/**
 * Задание.
 *
 * Создать элемент h1 с текстом «Нажмите любую клавишу.».
 *
 * При нажатии любой клавиши клавиатуры менять текст элемента h1 на:
 * «Нажатая клавиша: ИМЯ_КЛАВИШИ».
 */
// const h1 = document.createElement('h1')
// h1.innerText = 'Enter anybody button'
// document.body.append(h1)
//
// document.addEventListener("keyup", event => {
//     h1.innerText = `Your button: ${event.code}`
// })

/**
 * При натисканні shift та "+"" одночасно збільшувати
 * шрифт сторінки на 1px
 * а при shift та "-" - зменшувати на 1px
 *
 * Максимальний розмір шрифту - 30px, мінімальний - 10px
 *
 */


let fontSize = 16;
document.addEventListener('keyup', event => {
   if (event.key === '+'&& event.shiftKey ){
       if(fontSize < 30){
           fontSize++
           document.body.style.fontSize = `${fontSize}px`
       }
   }else if((event.key === '-' || event.key ==='_') && event.shiftKey){
       if(fontSize > 10 ){
           fontSize--
           document.body.style.fontSize = `${fontSize}px`
       }
   }
   if(event.key === '0' && event.ctrlKey){
       fontSize = 16;
       document.body.style.fontSize = `${fontSize}px`
   }
})









// let fontSize = 16;
// window.addEventListener("keyup", function (event) {
//     if (event.key === "+" && event.shiftKey) {
//         if (fontSize < 30) {
//             fontSize++;
//             document.body.style.fontSize = `${fontSize}px`;
//         }
//     }
// });