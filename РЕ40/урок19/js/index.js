// // /**
// //  * Задание 4.
// //  *
// //  * Написать функцию-помощник кладовщика.
// //  *
// //  * Функция обладает одним параметром:
// //  * - Строка со списком товаров через запятую (water,banana,black,tea,apple).
// //  *
// //  * Функция возвращает строку в формате ключ-значение, где ключ — имя товара, а значение — его остаток на складе.
// //  * Каждый новый товар внутри строки должен содержатся на новой строке.
// //  *
// //  * Если какого-то товара на складе нет, в качестве остатка указать «not found».
// //  *
// //  * Условия:
// //  * - Имя товара не должны быть чувствительно к регистру;
// //  * - Дополнительных проверок совершать не нужно.
// //  */
// //
// // /* Дано */
// //
// //
// // /* Решение */
// //
// // // function getStoreBalance(list)
// // // const getStoreBalance = list =>{
// // //     let result = '';
// // //     let currentWord = '';
// // //     let currentIndex = 0;
// // //     let nextIndex = 0;
// // //     list=list.toLowerCase()
// // //     while (nextIndex !== -1) {
// // //         nextIndex = list.indexOf(',' , currentIndex);
// // //         if (nextIndex===-1){
// // //             currentWord = list.slice(currentIndex);
// // //         }
// // //         else {
// // //             currentWord = list.slice (currentIndex,nextIndex);
// // //         }
// // //         let string =
// // //             result += `
// // //         ${currentWord} - ${store[currentWord] !== undefined ? store[currentWord]: "not found" }
// // //         `
// // //         currentIndex = nextIndex + 1;
// // //     }
// // //     return result;
// // // }
// const store = {
//     apple: 8,
//     beef: 162,
//     banana: 14,
//     chocolate: 0,
//     milk: 2,
//     water: 16,
//     coffee: 0,
//     tea: 13,
//     cheese: 0,
// };
// const getStoreBalance = list => {
//     let resultString = '';
//     let array = list.split(',');
//     array.forEach(item => {
//       let lowerCased = item.toLowerCase();
//       if (store[key]){
//           let string = `
//           ${key} - ${store[key]}
//           `
//           console.log(string)
//       } else {
//           string =`
//           ${key} - not found
//           `;
//           console.log(string)
//       }
//     })
//     return resultString
// }
// getStoreBalance()
// console.log(getStoreBalance('apPle,box,chEese,wAter,waLter,milk'));
// /**
//  * Задача 1.
//  *
//  * Дан массив с днями недели.
//  *
//  * Вывести в консоль все дни недели с помощью:
//  * - Классического цикла for;
//  * - Цикла for...of;
//  * - Специализированного метода для перебора элементов массива forEach.
//  */
//
// /* Дано */
// const days = [
//     'Monday',
//     'Tuesday',
//     'Wednesday',
//     'Thursday',
//     'Friday',
//     'Saturday',
//     'Sunday',
// ];
// for(let i = 0; i < days.length; i++){
//     console.log(days[i])
// }
//
// for (let x of days){
//     console.log(x)
// }
// days.forEach(element =>
//     console.log(element)
// )




// Создать массив чисел от 1 до 100.
// Отфильтровать его таким образом, чтобы в новый массив не попали числа меньше 10 и больше 50.
// Вывести в консоль новый массив.
// let array = [];
// for(let i = 0; i <= 100; i++){
//  array.push(i);
// }
// console.log(array.filter(number => number >= 10 && number <= 50))



// Описание задачи: Напишите функцию, которая очищает массив от нежелательных значений,
// таких как false, undefined, пустые строки, ноль, null.
//
// const data = [0, 1, false, 2, undefined, '', 3, null];
// const compact = data => data.filter(item => Boolean(item))
// console.log(compact(data)) // [1, 2, 3]




// Есть массив брендов автомобилей ["bMw", "Audi", "teSLa", "toYOTA"].
// Вам нужно получить новый массив, объектов типа
// {
//     type: 'car'
//     brand: ${элемент массива}
// }
// Вывести массив в консоль
// let brandAuto = ["bMw", "Audi", "teSLa", "toYOTA"]
// let array = brandAuto.map(item =>({
//         type: 'car',
//         brand:item.toUpperCase(),
// }))
// console.log(array)


// /**
//  * Задание 1.
//  *
//  * Написать функцию-сумматор.
//  *
//  * Функция обладает двумя числовыми параметрами, и возвращает результат их сложения.
//  *
//  * Условия:
//  * - Использовать функцию типа arrow function;
//  */
// // const calc = (a,b) => a + b
// // console.log(calc(5,4))

// /**
//  * Задача 3.
//  *
//  * Напишите функцию mergeArrays для объединения нескольких массивов в один.
//  *
//  * Функция обладает неограниченным количеством параметров.
//  * Функция возвращает один массив, который является сборным из массивов,
//  * переданных функции в качестве аргументов при её вызове.
//  *
//  * Условия:
//  * - Все аргументы функции должны обладать типом «массив», иначе генерировать ошибку;
//  * - В ошибке обязательно указать какой по счёту аргумент провоцирует ошибку.
//  *
//  * Заметки:
//  * - Делать поддержку выравнивания вложенных массивов (флеттенинг) не нужно.
//  */
// function mergeArrays(){
//     const array = Array.from(arguments);
//     const resultArray = [];
//     array.forEach((item,index) => {
//         if(Array.isArray(item)){
//             resultArray.push(...item)
//         }else {
//             throw new Error(`argument ${index + 1} is not array`)
//         }
//     })
//     return resultArray
// }
// console.log(mergeArrays([1, 2, 3], [4, 5, 6], ['Hello'], [' ', 'world', '!']));
// console.log(mergeArrays([1], 'error!'));


function sumTo(n) {
    let sum = 0;
    for (let i = 1; i <= n; i++) {
        sum += i;
    }
    return sum;
}

alert( sumTo(365) );