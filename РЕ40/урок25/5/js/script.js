/**
 * Задание 1.
 *
 * Написать программу для напоминаний.
 *
 * Все модальные окна реализовать через alert.
 *
 * Условия:
 * - Если пользователь не ввёл сообщение для напоминания — вывести alert с
 * сообщением «Ведите текст напоминания.»;
 * - Если пользователь не ввёл значение секунд,через сколько нужно вывести напоминание —
 *   вывести alert с сообщением «Время задержки должно быть больше одной секунды.»;
 * - Если все данные введены верно, при клике по кнопке «Напомнить» необходимо
 * её блокировать так,
 *   чтобы повторный клик стал возможен после полного завершения текущего напоминания;
 * - После этого вернуть изначальные значения обоих полей;
 * - Создавать напоминание, если внутри одного из двух элементов input нажать
 * клавишу Enter;
 * - После загрузки страницы установить фокус в текстовый input.
 */

const reminder = document.querySelector('#reminder')
const seconds = document.querySelector('#seconds')
const button = document.querySelector('button')

const handler = () => {
    if (reminder.value === ''){
        alert('Enter text to remind')
        return null
    }
    if(seconds.value && +seconds.value <= 1){
        alert('Second have been > 1 second')
        return null
    }
    button.setAttribute('disabled','')
    setTimeout(() =>{
        alert(`${reminder.value}`)
        button.removeAttribute('disabled')
        seconds.value = 0;
        reminder.value = '';
    },seconds.value * 1000)
}
button.addEventListener('click', handler)