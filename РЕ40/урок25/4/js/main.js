const imageLink = prompt('enter the link to image')
const img = document.createElement('img')
img.setAttribute('src', imageLink)
document.body.append(img)

const blink = () => {
img.style.display = 'none'
    setTimeout( () => img.style.display = 'block', 1000 )
}
setInterval(blink, 2000)
