const labelForm = document.createElement('label')
document.body.append(labelForm)

const userLogin = document.createElement('input')
userLogin.setAttribute('id', 'login')
userLogin.placeholder = 'login'
userLogin.style = 'display: block'

const userPassword = document.createElement('input')
userPassword.type = 'password'
userPassword.placeholder = 'password'
userPassword.style = "display: block"
userPassword.setAttribute('id','password')

const button = document.createElement('button')
button.type = 'submit'
button.innerText = 'Confirm'

labelForm.append(userLogin,userPassword,button)

button.addEventListener('click', () => {
    localStorage.setItem('Login', userLogin)
    localStorage.setItem('password', userPassword)
})