/**
 * Реализовать форму ввода данных для пользователя
 * ФИО,
 * номер телефона,
 * почта,
 * возраст
 *
 * Данные сохранять в localStorage в виде объекта,
 * и при загрузке страницы - считывать их и заполнять форму этими данными
 * При сохранении имитировать отправку данных на сервер,
 * блокируя кнопку сохранения на 2 секунды.
 * */

const labelForm = document.createElement('label')
document.body.append(labelForm)

const inputAllName = document.createElement('input')
inputAllName.setAttribute('id', 'text')
inputAllName.placeholder = 'Your all Name'
inputAllName.style = 'display:block'

const inputPhoneNumber = document.createElement('input')
inputPhoneNumber.setAttribute('id', 'phone')
inputPhoneNumber.placeholder = 'Your phone number'
inputPhoneNumber.style = 'display:block'
inputPhoneNumber.type = 'phone'

const inputMail = document.createElement('input')
inputMail.setAttribute('id', 'email')
inputMail.placeholder = 'Your email'
inputMail.style = 'display:block'
inputMail.type = 'email'

const inputAge = document.createElement('input')
inputAge.setAttribute('id', 'number')
inputAge.placeholder = 'Your age'
inputAge.style = 'display:block'
inputAge.type = 'number'

const button = document.createElement('button')
button.type = 'submit'
button.innerText = 'Confirm'

labelForm.append(inputAllName,inputPhoneNumber,inputMail,inputAge,button)


const handler = () => {
    button.setAttribute('disabled', '')
    const user = {
        name:inputAllName.value,
        age:inputAge.value,
        phone:inputPhoneNumber.value,
        email:inputMail.value
    }
    localStorage.setItem('user', JSON.stringify(user))
    setTimeout(function (){
        button.removeAttribute('disabled')
    },2000)
}
window.addEventListener('load', event => {
    const userObj = localStorage.getItem('user')
    const parsed = JSON.parse(userObj)
    inputAllName.value = parsed.name
    inputMail.value = parsed.email
    inputAge.value = parsed.age
    inputPhoneNumber.value = parsed.phone
})
button.addEventListener('click', handler)