/**
 *
 * Создайте 3 объекта, каждый со своими методами и свойствами:
 *    товар - Ноутбуки,
 *    товар - Телефоны,
 *    товар - Стиральные машинки.
 *    Повторяющиеся методы и свойства вынесите в прототип.
 *
 * */

const Product = {
    get price () {
        return this._price
    },
    get name () {
        return `${this._name} ${this._fullName}`
    },
    get calcDelivery () {
        if(this._vats){
            return this.price * (1 + this._vats / 100)
        }
        else {
            return this._price * 1.2
        }
    }
}
const notebook = {
    _name: 'Apple',
    _fullName:'Macbook',
    article: 133,
    _price: 25000,
    _vats: 20
}
const mobile = {
    _name: 'Samsung',
    _fullName:'Galaxy',
    article: 143,
    _price: 15000,
    // _vats: 20
}

notebook.__proto__ = Product;
mobile.__proto__ = Product;
// console.log(notebook);
// console.log(mobile);
console.log(mobile.price);
console.log(mobile.name);
console.log(mobile.calcDelivery);