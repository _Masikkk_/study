    /**
     * Наявний функціонал для друку оцінок одного студента
     * Але зі збільшенням кількості студентів постало питання про
     * розширення його
     * Для цього необхідно створити функцію-конструктор Student,
     * яка буде створювати об'єкт студента та мати ті ж самі методи
     *
     * - створити за допомогою функції-конструктора ще 2х студентів
     * - вивести оцінки кожного за допомогою метода printGrades
     * - вивести середній бал кожного студента
     * - додати метод, який буде виводити оцінку по заданій технологій
     * наприклад getGrade('html') повинен виводити оцінку студента по html
     * - вивести в консоль оцінку по js першого студента та по python - третього
     *
     *
     * ADVANCED:
     * - створити окремо функцію getStudentWithHighestResults, яка буде виводити
     * ім'я та прізвище студента за найвищим середнім балом
     */
    function Student (name, surname, gender, marks){
        this._name = name;
        this._surname = surname;
        this._gender = gender;
        this._marks = marks;
    }
    Student.prototype.getMiddleGrade = function (){
        let counter = 0;
        let sum = 0;
        for (let item in this._marks) {
            sum += this._marks[item];
            counter++
        }
        return sum / counter
    }
    Student.prototype.printGrades = function (){
        let counter = 0;
        let sum = 0;
        for (let item in this._marks) {
            sum += this._marks[item];
            counter++
        }
        if (this._gender === 'male'){
            console.log(`${this._name} ${this._surname} получил средний балл ${sum/counter}`)
        } else {
            console.log(`${this._name} ${this._surname} получила средний балл ${sum/counter}`)
        }
    }
    Student.prototype.currentMark = function (value){
        console.log(`${value} : ${this._marks[value]}`)
    }
    const grades1 = {
        html: 12,
        css:5,
        js: 7,
        python: 2
    }
    const grades2 = {
        html: 5,
        css:10,
        js: 9,
        python: 4
    }
    const grades3 = {
        html: 6,
        css:9,
        js: 7,
        python: 11
    }
    const student1 = new Student('oleg', 'olegov', 'male', grades1)
    const student2 = new Student('olga', 'olegovna', 'female', grades2)
    const student3 = new Student('viktor', 'viktorov', 'male', grades3)
    student1.printGrades()
    student1.currentMark('js')
    student3.currentMark('python')

    const getStudentWithHighestResults = (...students) => {
        let allStudents = [...students]
        let maxGrade = 0;
        allStudents.forEach(item => {
            let currentGrade = item.getMiddleGrade();
            maxGrade = Math.max(currentGrade, maxGrade)
            if (maxGrade > currentGrade){
                console.log(item._name, maxGrade)
            }
            // console.log(item._name, maxGrade)

        })
    }
    getStudentWithHighestResults(student1, student2, student3)