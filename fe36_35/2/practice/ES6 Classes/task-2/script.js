/**
Напишите класс User, который будет использоваться для создания объекта, описывающего пользователя панели администрирования сайта. У объекта должны быть такие поля:
- role (super admin, admin, main manager, content manager);
- login;
- email;
- password;

сеттеры и геттеры:
- role (можно установить роль только из списка: super admin, admin, main manager, content manager);
- login (не меньше трех букв);

а также метод:
- isValidEmail - проверят, является ли переданная строка email'ом (в ней должен быть символ @, минимум 1 точка и не должно быть пробелов);

Дополнительное задание: добавьте метод getPasswordStrength, который проверяет силу пароля. Метод получает строку (пароль) и возвращает один из трех вариантов:
- weak - меньше 6 символов, не содержит букв или цифр;
- medium - больше 6 символов, содержит цифры и буквы, но не содержит знаков: -_^$%#@*&?
- strong - больше 6 символов, содержит буквы, цифры и спецсимволы: -_^$%#@*&?
**/
class User {
    // constructor(role,login,email,password) {
    constructor() {
        this._role = null;
        this._login = null;
        this._email = null;
        this._password = null;
        this.passwordStatus = null;

    }

    set role( value ) {
        const roles = [ "super admin", "admin", "main manager", "content manager" ];
        if ( roles.includes( value ) ) {
            this._role = value
        }
    }

    set login( value ) {
        if ( value.length >= 3 ) {
            this._login = value
        }
    }

    set email( value ) {
        if ( this.isValidEmail( value ) ) {
            this._email = value
        }
    }

    set password( value ) {
        this.getPasswordStrength( value );
        this._password = value;
    }

    isValidEmail( value ) {
        return value.includes( "@" ) && value.includes( "." ) && !value.includes( " " )
    }

    get email() {
        return this._email
    }

    getPasswordStrength( value ) {

        const numbers_letters_regexp = /\d\w/; // числа и буквы
        const special_characters_regexp = /[-_^$%#@*&?]/g; // набор -_^$%#@*&?

        if ( value.length < 6 && !numbers_letters_regexp.test( value ) ) {
            this.passwordStatus = 'weak';
        } else if ( value.length > 6 && numbers_letters_regexp.test( value ) && !special_characters_regexp.test( value ) ) {
            this.passwordStatus = 'medium';
        } else if ( value.length > 6 && numbers_letters_regexp.test( value ) && special_characters_regexp.test( value ) ) {
            this.passwordStatus = 'strong';
        } else {
            this.passwordStatus = '`((';
        }
    }

}

const admin = new User();

admin.password = 'dssddsdsd3333sasdsadsa';
console.log( admin );

admin.password = 'dss#$%^dsdsd3333sasdsadsa';
console.log( admin );

//https://learn.javascript.ru/regular-expressions

console.log(admin);