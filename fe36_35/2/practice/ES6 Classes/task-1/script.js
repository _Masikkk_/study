/**
 *
 * За допомогою класів створити наступні об'єкти: 2 ноутбуки, 3 телефони та 1 планшет
 *
 * Вивести в консоль вартість для ринку 3х будь-яких товарів
 * Вивести в консоль вартість доставки для 3х різних товарів
 *
 * ### ADVANCED
 * Додати можливість створювати преміум телефони, які мають таку ж логіку, як і телефони, тільки націнка становить 35%
 *
 * ## Технічні вимоги
 * При створенні задається виробник, модель та ціна
 *
 * Метод getName() повинен повертати повну назву продукту:
 * ВИРОБНИК + МОДЕЛЬ, наприклад Apple Macbook Pro 13"
 *
 * Для телефонів також задається наявність NFC чипу,
 * а для планшетів наявність слоту для sim карти
 *
 * Кожен продукт має націнку 20%, додати метод, який
 * буде повертати ціну вже з націнкою
 *
 * ### Розрахунок вартості доставки
 * Кожен тип продукту (ноутбук, планшет та телефон) має свою логіку розрахунку вартості доставки:
 *
 * #### Для НОУТБУКУ
 * якщо ціна з націнкою більше 30 000, то доставка безкоштовна
 * інакше доставка коштує 200
 *
 * #### Для ПЛАНШЕТУ
 * якщо ціна з націнкою більше 20 000, то доставка безкоштовна
 * інакше ціна доставки це 1% від ціни з націнкою
 *
 * #### Для ТЕЛЕФОНУ
 * якщо ціна з націнкою більше 10 000, то доставка розраховується як 1% від загальної ціни
 * інакше береться 3%
 * 
 * */
class Product {
    constructor(manufacture, model, price, vats) {
        this.manufacture = manufacture;
        this.model = model;
        this.price = price;
        this.vats = vats;
    }
    getName(){
        return `${this.manufacture} ${this.model}`
    }
    getPriceIncludesVats (){
       return this.price * (1+this.vats/100);
    }
}
const notebook = new Product('Apple', 'Macbook Pro13\"', 31000, 20)
console.log(notebook);
console.log(notebook.getName());
console.log(notebook.getPriceIncludesVats());

class Laptop extends Product {
    constructor(manufacture, model, price, vats) {
        super(manufacture, model, price,vats);
    }
    calcDeliveryCost () {
        if (this.getPriceIncludesVats() <= 30000){
            return 200
        }
        else{
            return 'free'
        }
    }
}

const notebookApple = new Laptop('Apple', 'Macbook Pro13"', 28000, 20)
console.log(notebookApple);
console.log(notebookApple.calcDeliveryCost());
console.log(notebookApple.getPriceIncludesVats());

 class Tablet extends Product {
     constructor(manufacture, model, price,vats) {
         super(manufacture, model, price,vats);
     }
     calcDeliveryCost () {
         if (this.getPriceIncludesVats() <= 20000){
             return this.getPriceIncludesVats() * 0.01
         }
         else{
             return 'free'
         }
     }
 }
const tablet = new Tablet('Apple', 'Macbook Pro13"', 15000, 20)
console.log(tablet);
console.log(tablet.getPriceIncludesVats());
console.log(tablet.calcDeliveryCost());

class Mobile extends  Product {
    constructor(manufacture, model, price,vats, nfc) {
        super(manufacture, model, price,vats);
        this.nfc = nfc;
    }
    calcDeliveryCost () {
        if (this.getPriceIncludesVats() >= 10000){
            return this.price * 0.01
        }
        else{
            return this.price * 0.03
        }
    }
}
// const mobile = new Mobile('Apple', 'Iphone Pro13', 9000, true)
// console.log(mobile);
// console.log(mobile.getPriceIncludesVats());
// console.log(mobile.calcDeliveryCost());
const mobile = new Mobile ('Apple', 'Iphone Pro13', 6000, 35,true)
console.log(mobile);
console.log(mobile.getPriceIncludesVats());
console.log(mobile.calcDeliveryCost());