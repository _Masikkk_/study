/**
 * productsPromise робить запит на сервер, і іноді з'являється помилка
 *
 * Якщо продукти приходять, то вивести їх списком на сторінку,
 * інакше вивести модальне вікно з ім'ям помилки (alert)
 *
 */
 const productPromise = new Promise((resolve,reject) => {
     setTimeout(() => {
         if (Math.random() < 0.5){
             resolve (['apple', 'orange', 'watermelon','lemon'])
         } else {
             reject (new Error('Error'))
         }
     }, 2000)
})
const loader = document.querySelector('.loader')
productPromise
.then(products => {

    const listUl = document.createElement('ul')
    const li = products.map((item) => `<li>${item}</li>`)
    listUl.innerHTML = li.join('')
    document.body.append(listUl)
    loader.remove()
})
.catch(error => {
    loader.innerText = 'Error';
    alert(error.message)
})