/**
 * Написати функціонал логіну на сайт
 * при натисканні на кнопку `Login` перевіряються введені дані
 * через 1 секунду, якщо дані валідні, то виводиться модальне вікно з текстом Success
 * інакше в блок помилок на сторінці виводиться повідомлення _Invalid data_

 ## Технічні вимоги
 * При натисканні на `Login` кнопку передається функції `authenticate` ім'я користувача та пароль
 * Через вказаний проміжок часу резолвиться або реджектиться проміс відповідно без або з помилкою
 * Використати наявний масив валідних даних для перевірки
 *
 * */
const USERS = [
	['admin', '123'],
	['john', 'qwe'],
	['jack', 'asd'],
	['marry', '123']
]

function authorization(userName, userPassword) {
	return new Promise((resolve, reject) => {
		setTimeout(() => {
			const user = USERS.find(login => login[0] === userName && login[1] === userPassword)
			if (user) {
				resolve([userName])
			} else {
				reject(new Error(`Неверный логин или пароль`))
			}
		}, 1000)
	})
}

function wrapper(userName, userPassword, errorElem) {
	authorization(userName, userPassword)
		.then((data) => {
			alert(data[0])
			errorElem.innerText = ""
		})
		.catch(error => errorElem.innerText = error.message)
}
const loginInput = document.querySelector('[name="username"]')
const passwordInput = document.querySelector('[name="password"]')
const errorMessage = document.querySelector('.errors')
const buttonLogin = document.querySelector('.submit_login')
buttonLogin.addEventListener("click", () => {
	wrapper(loginInput.value, passwordInput.value, errorMessage)
})