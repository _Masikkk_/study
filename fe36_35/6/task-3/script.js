/**
 * Написати функцію getWeather, яка приймає ім'я міста та
 * повертає проміс, який через 1 секунду зарезолвиться,
 * якщо ми передали Kyiv, Lviv, Kharkiv,
 * інакше буде помилка з текстом 'Невідоме місто'
 *
 */




  // @param {*} city - Kyiv


function getWeather (city) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const cities = ['Kyiv', 'Lviv', 'Kharkiv']
            if (cities.includes(city)) {
                resolve([true,{
                    weather: 'сонячна',
                    temperature: '9 C',
                    city: city
                }])
                // resolve([
                //     {weather:'сонячна'},
                //     {temperature: '9 C'},
                //     {city:city}
                // ])
                // resolve(['сонячна', '9 C', `${city}`])
            }else {
                reject (new Error('Невідоме місто'))
            }
        }, 1000)
    })
}
function wrapper (city) {
    getWeather(city)
        .then((data) => {
            // let [weather,temperature,city] = data;
            // let [{weather}, {temperature}, {city}] = data;
            // const [, {weather, temperature, city}] = data;
            const newData = data.map((el)=>{
                if (typeof el !== "boolean"){
                    // console.log(el);
                    console.log(`${el.city} temperature : ${el.temperature} weather: ${el.weather}`);

                }
            })
            // console.log(`${city} temperature : ${temperature} weather: ${weather}`);
        })
        .catch(error => console.log(error.message))
}
wrapper('Kyiv')
wrapper('Sumy')