import path from "../gulpfile.babel.js/config/path.js";

import del from "del";

const clear = () => {
  return del(path.root)
}

export default clear