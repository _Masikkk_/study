const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const gulpClean = require('gulp-clean')
const concat = require('gulp-concat')
const minCss = require('gulp-cssmin')

gulp.task('moveJs',()=>{
    return gulp.src('src/scripts/*.js')
        .pipe(gulp.dest('dist/js'))
})
gulp.task('moveImg', ()=>{
    return gulp.src('src/image/*')
        .pipe(gulp.dest('dist/image'))
})
gulp.task('moveCss', ()=>{
    return gulp.src('src/css/*.css')
        .pipe(gulp.dest('dist/css'))
})

const scss = () => {
    return gulp.src('src/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('src/css'))
}

gulp.task('scss', scss);

gulp.task('clean', ()=>{
    return gulp.src('dist/*')
        .pipe(gulpClean())
})
gulp.task('build', gulp.series('clean', 'moveJs',  'moveImg', 'scss', 'moveCss'));

const path = gulp.series('clean','moveJs', 'moveImg','scss',  'moveCss');

const watch = () => {
    gulp.watch('src/image/*', path);
    gulp.watch('src/scripts/*.js', path);
    gulp.watch('src/css/*.css', path);
    gulp.watch('src/scss/**/*.scss', path);
}

gulp.task('watch', gulp.series('build', watch));


gulp.task('processCss', ()=>{
    return gulp.src('src/css/*.css')
        .pipe(concat('style.min.css'))
        .pipe(minCss())
        .pipe(gulp.dest('dist/css'))
})
//
// gulp.task('watch', () => {
//   gulp.watch('scss/*.scss', scss)
// })
// gulp.task('watch', gulp.series('build', watch));