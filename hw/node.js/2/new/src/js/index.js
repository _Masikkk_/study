let burgerBtn = document.querySelector('.burger-btn');
let navBar = document.querySelector('.menu__list');

burgerBtn.addEventListener('click', function(){
    burgerBtn.classList.toggle('active');

    if(navBar.style.top === '60px'){
        navBar.style.top = '-300px';
    } else{
        navBar.style.top = '60px';
    }
})
