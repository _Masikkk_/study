const gulpClean = require('gulp-clean')
const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const concat = require('gulp-concat');
const minifyJs = require('gulp-js-minify');
const imagemin = require('gulp-imagemin');
const cleanCSS = require('gulp-clean-css');
const autoprefixer = require('gulp-autoprefixer');
const uglify = require('gulp-uglify')
const browserSync = require('browser-sync').create();

function scripts() {
    return gulp.src('src/scripts/*.js')
        .pipe(concat('scripts.min.js'))
        .pipe(uglify())
        .pipe(minifyJs())
        .pipe(gulp.dest('dist/scripts/'))
        .pipe(browserSync.stream());
}

function styles() {
    return gulp.src('src/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('styles.min.css'))
        .pipe(autoprefixer({
            overrideBrowserslist: ['last 2 versions'],
            cascade: false
        }))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('dist/css/'))
        .pipe(browserSync.stream());
}

function imgMin() {
    return gulp.src('src/image/*.png')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/image/'))
}

function clean() {
    return gulp.src('dist/*')
        .pipe(gulpClean())
}

gulp.task('clean', clean)
gulp.task('scripts', scripts)
gulp.task('styles', styles)
gulp.task('imgMin', imgMin)

gulp.task('build', gulp.series(clean,
    gulp.parallel(styles, scripts, imgMin)));


const path = gulp.series(clean,scripts, imgMin,styles);

const watch = () => {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });

    gulp.watch('src/image/*', path);
    gulp.watch('src/scripts/*.js', path);
    gulp.watch('src/scss/**/*.scss', path);
    gulp.watch('./*.html', browserSync.reload);

}

gulp.task('dev',watch);
