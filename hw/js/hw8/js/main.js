const div = document.createElement('div')
document.body.append(div)

let userInput = document.createElement('input')
userInput.style.outline = 'none'
userInput.placeholder = 'price'
userInput.setAttribute('id', 'price')
userInput.type = 'number'
document.body.append(userInput)

let errorElement = null

userInput.addEventListener('focus', event => {
    event.target.style.border = '2px solid green'
})

userInput.addEventListener('blur', event => {
    event.target.style.removeProperty('border')
    const result = +event.target.value
    if (result < 0) {
        event.target.style.border = '2px solid red'
        userInput.style.removeProperty('color')
        if (!errorElement) {
            errorElement = document.createElement('p')
            errorElement.innerText = 'Please enter correct price'
            document.body.append(errorElement)
        }
    } else {
        const span = document.createElement('span')
        span.innerText = `Current price: ${result}`
        span.style.border = '1px solid grey'
        span.style.borderRadius = '25px'
        span.style.color = '#1E90FF'
        span.style.backgroundColor = 'darkgrey'
        const button = document.createElement('button')
        button.innerText = 'X'
        button.style.borderRadius = '25px'
        button.style.color = 'red'
        button.style.backgroundColor = 'pink'
        button.style.border = '1px solid red'
        button.style.position = 'relative'
        button.style.bottom = '0.9px'
        button.style.right = '-1px'
        span.append(button)
        button.addEventListener('click', () => {
            span.remove()
            userInput.value = '';
        })

        div.append(span)
        userInput.style.color = 'green'
        if (errorElement) {
            errorElement.remove()
            errorElement = null
        }
        userInput.addEventListener('click', () => {
            span.remove()
            errorElement.remove()
            errorElement = null
        })
    }

})