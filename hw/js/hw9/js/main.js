const tabs = document.querySelector('.tabs')
const tabsContent = document.querySelector('.tabs-content')

tabs.addEventListener('click', e => {
    if (!e.target.classList.contains('tabs-title') || e.target.classList.contains('active')) return

    tabs.querySelector('.active').classList.remove('active')
    tabsContent.querySelector('.active').classList.remove('active')

    e.target.classList.add('active')
    const item = e.target.dataset.item;
    tabsContent.querySelector(`[data-item="${item}"]`).classList.add('active')
})