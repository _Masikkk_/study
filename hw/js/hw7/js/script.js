createList = (arr, element = document.body) => {
    arr.map(function (value) {
        let ulList = document.createElement('ul')
        let listItem = document.createElement('li')
        if (Array.isArray(value)) {
            element.append(ulList)
            createList(value, ulList)
        } else {
            listItem.innerText = value;
            element.append(listItem)
        }

    })
}
createList(["hello", "world", ["Kiev","Kharkiv"], "Odessa", "Lviv"])

