const btn = document.querySelector('button');

const input1 = document.getElementById('input1')
const input2 = document.getElementById('input2');

const icon1= document.getElementById('icon1');
const icon2= document.getElementById('icon2');

const errorText = document.createElement('p')

errorText.textContent = 'Нужно ввести одинаковые значения';
errorText.style.color = 'red';
errorText.style.width = '300px';
errorText.style.marginBottom = '10px';

btn.addEventListener('click', (event) =>{
    event.preventDefault()
    if (input1.value !== input2.value || input1.value === '' || input2.value === ''){
        input2.after(errorText);
    }else {
        alert('You are welcome')
        input1.value = '';
        input2.value = '';
        errorText.remove()
    }
})
btn.style.cursor = 'pointer'
icon1.addEventListener('click', () => {
    if(input1.getAttribute('type') === 'password'){
        icon1.className = 'fas fa-eye-slash icon-password';
        input1.setAttribute('type', 'text')
    }else {
        icon1.className = 'fas fa-eye icon-password';
        input1.setAttribute('type', 'password')
    }
});

icon2.addEventListener('click', () => {
    if(input2.getAttribute('type') === 'password'){
        icon2.className = 'fas fa-eye-slash icon-password';
        input2.setAttribute('type', 'text')
    }else {
        icon2.className = 'fas fa-eye icon-password';
        input2.setAttribute('type', 'password')
    }
});
