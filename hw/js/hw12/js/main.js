const animationDuration = 500
let interval = 5 * 1000
const images = document.querySelectorAll('.image-to-show')
let intervalId = null
let activeIndex = null
let stopBtn = null
let resumeBtn = null



const showImage = (nextIndex) => {
    const nextEl = images[nextIndex]

    if (activeIndex !== null) {
        const prevEl = images[activeIndex]

        prevEl.animate(
            [
                {opacity: 1},
                {opacity: 0},
            ],
            animationDuration
        )

        setTimeout(() => {
            prevEl.classList.remove('active')

            nextEl.animate(
                [
                    {opacity: 0},
                    {opacity: 1},
                ],
                animationDuration
            )
            nextEl.classList.add('active')
        }, animationDuration)
    } else {
        nextEl.classList.add('active')
    }

    activeIndex = nextIndex

    let timer;
    let x = 4;
    function countDown(){
        document.getElementById('timer').innerHTML = `timer:${x}`;
        x--;
        if (x<0){
            clearTimeout(timer);
        }
        else {
            timer = setTimeout(countDown, 1000);
        }
    }
    countDown()
}

const startLoop = () => {
    resumeBtn.disabled = true
    stopBtn.disabled = false
    intervalId = setInterval(() => {
        const nextIndex = activeIndex === images.length - 1 ? 0 : activeIndex + 1
        showImage(nextIndex)
    }, interval - animationDuration)

}

const stopLoop = () => {
    resumeBtn.disabled = false
    stopBtn.disabled = true
    clearInterval(intervalId)
    intervalId = null
}

showImage(0)

stopBtn = document.createElement('button')
stopBtn.innerText = 'Stop'
stopBtn.addEventListener('click', stopLoop)

resumeBtn = document.createElement('button')
resumeBtn.innerText = 'Resume'
resumeBtn.addEventListener('click', startLoop)

document.body.append(stopBtn, resumeBtn)

startLoop()


