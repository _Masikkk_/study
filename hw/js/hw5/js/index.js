const createUser = () => {
    do {
        firstName = prompt('Enter your first name');
        lastName = prompt('Enter your last name');
        birthday = prompt('Enter your birth date like dd.mm.yyyy');
    } while (!firstName || !lastName || typeof firstName == 'undefined' || typeof lastName == 'undefined');
    let newUser = {
        _firstUserName:firstName,
        set firstUserName (value1){
            this._firstUserName = value1
        },
        get firstUserName (){
            return this._firstUserName
        },
        _lastUserName:lastName,
        set lastUserName (value2){
            this._lastUserName = value2
        },
        get lastUserName (){
            return this._lastUserName
        },
        birthday: birthday,
        getLogin: () => {console.log('Your login: ' + firstName.charAt(0).toLowerCase() + lastName.toLowerCase()) },
        getAge(date) {
                birthdayDate  = new Date(this.birthday.split(".").reverse().join('-'));
                return Math.floor( ( (new Date() - birthdayDate) / 1000 / (60 * 60 * 24) ) / 365.25 );
        },
        getPassword:() => {
            console.log('Your password: ' + firstName[0].toUpperCase() + lastName.toLowerCase() + this.birthday.slice(6,11))
        },
    };
    return newUser;
};
const user10 = createUser();
user10.getLogin();
user10.getPassword();
console.log('Your age: ' + user10.getAge());