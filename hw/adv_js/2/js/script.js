const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];
const div = document.getElementById('root')
books.forEach(i => {
    try {
        if (!i.hasOwnProperty('author')) {
            throw new SyntaxError(i.name + " don't have a author")
        }
        if (!i.hasOwnProperty('name')) {
            throw new SyntaxError(i.name + " don't have a name")
        }
        if (!i.hasOwnProperty('price')) {
            throw new SyntaxError(i.name + " don't have a price")
        }

        let bookList = document.createElement('ul')
        let listEl = document.createElement('li')

        div.append(bookList)
        bookList.appendChild(listEl)
        listEl.innerText = `Название: ${i.name}; 
        Автор:${i.author};
        Цена:${i.price}.`
    } catch (err) {
        console.log(err.message)
    }
})
