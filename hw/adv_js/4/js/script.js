const getData = (url) => {
    return fetch(url)
        .then(response => response.json())
}

const renderFilms = () => {
    const ul = document.createElement('ul')
    document.body.append(ul)
    getData('https://ajax.test-danit.com/api/swapi/films')
        .then(films => {
            films.forEach(film => {
                const filmElement = `<li>
                    <span style='color:red'>Episode: ${film.episodeId}</span>, 
                    <b>${film.name}</b>
                    <p> Opening Crawl: ${film.openingCrawl}</p>
                    </li>
                    <li id='characters${film.id}' style='list-style:none'>
                    <div class="loader" data-animation=${film.id}></div>
                    </li>`
                ul.insertAdjacentHTML('beforebegin',filmElement)
            })
        })
}

const renderCharacters = () => {
    getData('https://ajax.test-danit.com/api/swapi/films')
        .then(films => {
            films.forEach(film => {
                const charactersElement = document.querySelector(`#characters${film.id}`)
                const animationElement = document.querySelector(`[data-animation='${film.id}']`)
                const innerOl = document.createElement('ol')
                charactersElement.append(innerOl)

                const charactersUrls = film.characters;
                const requestCharacters = charactersUrls.map(url => fetch(url))
                Promise.all(requestCharacters)
                    .then(responses => Promise.all(responses.map(response => response.json())))
                    .then(characters => {
                        characters.forEach(character => {
                            const characterInfo =
                                `<li>
							${character.name},
							gender: ${character.gender ? character.gender : 'no gender'},
							birth year: ${character.birthYear ? character.birthYear : 'unknown'},
							height: ${character.height ? character.height : 'height unknown'},
							mass: ${character.mass ? character.mass : 'mass unknown'},
							eye color: ${character.eyeColor ? character.eyeColor : 'no eyes'},
							skin color: ${character.skinColor ? character.skinColor : 'no skin'},
							hair color: ${character.hairColor ? character.hairColor : 'no hair'}
							</li>`
                            animationElement.remove()
                            innerOl.insertAdjacentHTML('beforebegin', characterInfo)
                        })
                    })
            })
        })
}

const display = () => {
    renderFilms()
    renderCharacters()
}

display()