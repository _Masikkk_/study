class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get age () {
        return this._age
    }
    get salary () {
        return this._salary
    }
    get name () {
        return this._name
    }
    set name(value) {
        this._name = value;
    }
    set age(value) {
        this._age = value;
    }
    set salary(value) {
        this._salary = value;
    }

}

class Programmer extends Employee{
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang= lang;
    }
    get lang () {
        return this._lang
    }
    set lang(value) {
        this._lang = value;
    }
    get salary () {
        return this._salary*3
    }
    set salary(value) {
        this._salary = value;
    }
}


const junior = new Programmer('Victor', 15, 7000, 'java')
const middle = new Programmer('Vanya', 24, 5000, 'C#')
const senior = new Programmer('Vlad', 18, 10000, 'js')

junior.salary = 2000
console.log(junior, middle, senior)