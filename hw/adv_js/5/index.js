import Card from "./js/card.js"
import Modal from "./js/modal.js"

const cards = new Card()
cards.renderCards()

const modal = new Modal()
modal.renderModal()
modal.addNewPost()
