import React from 'react';
import { useSelector } from 'react-redux';
import Product from '../../components/Product/Product';
import './cartPage.scss';

const CartPage = () => {
	const cart = useSelector(state => state.cart)

	return (
		<div className='cart'>
			<div className='cart__wrapper'>
				{cart.length === 0 &&
					<div className='cart-empty'>No items have been added to cart.</div>}
				{cart.length > 0 &&
					cart.map((cartItem) => {
						return <Product
							key={`${cartItem.SKU}C`}
							product={cartItem}
							inCart={true} />
					})
				}
			</div>
		</div>
	);
};

export default CartPage;