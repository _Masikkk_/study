import React from "react";
import "./ActionButton.css"


const ActionButton = ({backgroundColor, text, fn}) => {

    return(
        <button
            className={"action-btn"}
            style={{ backgroundColor: backgroundColor}}
            onClick={ fn }
        >
            {text}
        </button>


    );
};

export default ActionButton;