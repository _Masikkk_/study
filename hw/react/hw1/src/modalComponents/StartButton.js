import React from "react";
import "./StartButton.css"

function StartButton(props) {
    return <button className="open-btn" onClick={props.onClick}>{props.text}</button>;
}

export default StartButton;