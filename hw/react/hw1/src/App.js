import React, {useState} from "react";

import "./App.css";
import Modal from "./modalComponents/Modal"
import ActionButton from "./modalComponents/ActionButton"
import StartButton from "./modalComponents/StartButton"


function App() {
    const [modalActive, setModalActive] = useState('');

  return (
      <div className="app">

        <main>

            <StartButton text="First Modal Window" onClick={() => setModalActive("modal-1")} />
            <StartButton text="Second Modal Window" onClick={() => setModalActive("modal-2")} />

        </main>

            <Modal
                id="modal-1"
                active={modalActive}
                 setActive={setModalActive}
                 header={"First modal"}
                 closeButton={true}
                 text={"Is it you again?"}
                   actions={(<div>
                       <ActionButton
                           text={"Yes"}
                           backgroundColor={"green"}
                           fn={() => alert("Hi! Welcome!")} />
                       <ActionButton
                           text={"No"}
                           fn={() => alert("Oh, nice to meet you.Welcome!")}
                           backgroundColor={"red"}/>
                   </div>)} >
            </Modal>

            <Modal id="modal-2"
                   active={modalActive}
                 setActive={setModalActive}
                 header={"Second modal"}
                 closeButton={false}
                 text={"My name is..."}
                 actions={(<div>
                     <ActionButton
                         text={"Max"}
                         backgroundColor={"red"}
                         fn={() => alert("Yes, you are right")} />

                     <ActionButton
                         text={"Vlad"}
                         fn={() => alert("No, you made a mistake")}
                         backgroundColor={"green"}/>

                 </div>)} >
            </Modal>

      </div>
  )
}

export default App;