import './header.scss';

const Header = () => {
	return (
		<div className='header'>
			<div className='header__text'>Buy</div>
			<img className='header__logo' src='img/phone.svg' alt='black phone' />
			<div className='header__text'>Phones</div>
		</div>
	);
}

export default Header;