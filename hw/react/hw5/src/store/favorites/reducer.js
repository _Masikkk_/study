import types from "../types";

const favoritesReducer = (state = [], action) => {
	switch (action.type) {
		case types.setFavorites: {
			return action.payload.favorites
		}
		default: {
			return state
		}
	}
}

export default favoritesReducer