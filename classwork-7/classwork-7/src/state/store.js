import {reducer as filmsReducer} from "./films";
import {reducer as charactersReducer} from "./characters";
import {combineReducers, createStore} from "redux";

const reducer = combineReducers({
    films:filmsReducer,
    characters: charactersReducer,
})



const store = createStore(reducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())
export default store