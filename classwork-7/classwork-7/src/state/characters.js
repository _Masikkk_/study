export const STORE_PERSON = "STORE_PERSON";
const initialState = {
    items: {},
}

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case STORE_PERSON:
            return {...state, items: {...state.items, ...action.payload}}
        default:
            return state
    }
}