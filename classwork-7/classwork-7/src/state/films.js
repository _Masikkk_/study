
export const STORE_FILMS = "STORE_FILMS";
const initialState = {
    items: [],
}

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case STORE_FILMS:
            return {...state, items: action.payload}
        default:
            return state
    }
}
