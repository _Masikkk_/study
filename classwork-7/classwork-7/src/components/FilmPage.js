import Film from "./Film";
import {useSelector} from "react-redux";

function FilmPage(props) {
    const {match} = props;
    const {filmId} = match.params;

    const films = useSelector((state) => state.films.items);
    const film = films.find(({id}) => +filmId === +id);

    if (typeof film === "undefined") {
        return <div>Film is not found</div>
    }
    return <Film film={film}/>
}

export default FilmPage;