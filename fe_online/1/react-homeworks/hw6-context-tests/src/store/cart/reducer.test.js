import cartReducer from "./reducer";
import types from "../types";

describe('cart reducer', () => {
	const currentState = [{ SKU: 1, price: 10, name: 'Shark' }, { SKU: 2, price: 11, name: 'Cutter' }]
	const newState = [{ SKU: 2, price: 11, name: 'Cutter' }, { SKU: 3, price: 12, name: 'Boy' }]

	it('should return default value', () => {
		expect(cartReducer(currentState, {
			type: 'unknown type',
			payload: 'any data'
		})).toEqual(currentState)
	})

	it('should update cart', () => {
		expect(cartReducer(currentState, {
			type: types.setCart,
			payload: {
				cart: newState
			}
		})).toEqual(newState)
	})

	it('should clean cart after purchase confirmation', () => {
		expect(cartReducer(currentState, {
			type: types.confirmCartPurchase,
			payload: null
		})).toEqual([])
	})
})