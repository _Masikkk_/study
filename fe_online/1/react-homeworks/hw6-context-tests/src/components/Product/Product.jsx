import PropTypes from 'prop-types';
import './product.scss';
import { useDispatch, useSelector } from 'react-redux';
import { setFavorites } from '../../store/favorites/action';
import { setModal } from '../../store/modalInfo/action';
import classnames from 'classnames';
import ProductsViewContext from '../../context/ProductsViewContext';
import { useContext } from 'react';

const Product = (props) => {
	const { product, inCart, isFavorite } = props
	const favorites = useSelector(state => state.favorites)
	const dispatch = useDispatch()
	const { contextView } = useContext(ProductsViewContext)

	const toggleFavorites = (favoriteItem) => {
		const favoritesAdd = [...favorites, favoriteItem]
		const favoritesFiltered = favorites.filter(favorite => favorite.SKU !== favoriteItem.SKU)

		if (favorites.some((favorite) => favorite.SKU === favoriteItem.SKU)) {
			dispatch(setFavorites(favoritesFiltered))
			localStorage.setItem('favorites', JSON.stringify(favoritesFiltered))
		} else {
			dispatch(setFavorites(favoritesAdd))
			localStorage.setItem('favorites', JSON.stringify(favoritesAdd))
		}
	}

	const displayModal = (cartItem, type) => {
		dispatch(setModal({
			noModal: false,
			productData: cartItem,
			modalType: type,
		}))
	}

	return (
		<div className={classnames({
			'product-table-view': contextView === 'table',
			'product-cards-view': contextView === 'cards',
			'cart-product': inCart, 'favorite-product': isFavorite
		})}>
			{inCart &&
				<>
					<button onClick={() => displayModal(product, 'removeFromCart')}
						className='delete-from-cart'>X
					</button>
					<div className='cart-quantity'>
						<div className='quantity'>{product.quantity}</div>
					</div>
				</>
			}
			<h2 className={classnames({
				'product-table-view__name': contextView === 'table',
				'product-cards-view__name': contextView === 'cards',
			})}
				style={{ color: product.color }}>
				{product.name}
			</h2>
			<div className='product__img'>
				<img src={product.url} alt='knife' />
			</div>
			{
				inCart && product.quantity > 1 ?
					<div className='product__price'>Total price: {product.price * product.quantity} USD</div> :
					<div className='product__price'>Price: {product.price} USD</div>
			}
			<div className='product__action'>
				{favorites.some(favorite => favorite.SKU === product.SKU) ?
					<img onClick={() => toggleFavorites(product)}
						className={classnames({
							'product-table-view__favorites': contextView === 'table',
							'product-cards-view__favorites': contextView === 'cards'
						})}
						src='img/star-added.png'
						alt='yellow star' /> :
					<img onClick={() => toggleFavorites(product)}
						className={classnames({
							'product-table-view__favorites': contextView === 'table',
							'product-cards-view__favorites': contextView === 'cards'
						})}
						src='img/favorite-2765.svg'
						alt='black star' />
				}
				<button
					onClick={() => { displayModal(product, 'addToCart') }}
					className={classnames({
						'product__cart-hidden': inCart,
						'product-table-view__cart': contextView === 'table',
						'product-cards-view__cart': contextView === 'cards'
					})}>
					Add to cart
				</button>
			</div>
		</div >
	);
}

Product.defaultProps = {
	product: {
		name: 'best knife',
		color: 'orange',
		price: 1,
		url: 'img/Cutter.jpg'
	},
	inCart: false
}

Product.propTypes = {
	product: PropTypes.object,
	inCart: PropTypes.bool,
	isFavorite: PropTypes.bool
}

export default Product;