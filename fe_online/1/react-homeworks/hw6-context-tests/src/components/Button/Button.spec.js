import Button from "./Button";
import { render, screen } from '@testing-library/react'

describe('Button component', () => {
	it('should render button tag', () => {
		render(<Button></Button>)
		screen.getByRole('button')
	})

	it('should render button with children text', () => {
		const { container } = render(<Button>some text</Button>)
		const hasRequiredText = /some text/.test(container.innerHTML)
		expect(hasRequiredText).toBe(true)
	})
})