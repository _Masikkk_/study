import { useSelector } from 'react-redux';
import Product from '../Product/Product';
import './productsList.scss';

const ProductsList = () => {
	const products = useSelector(state => state.products)

	return (
			<div className='products'>
				<div className='products__wrapper'>
					{typeof products === 'string' ?
						<div className='products__message'>{products}</div> :
						products.map(product => {
							return <Product
								key={product.SKU}
								product={product}
							/>
						})
					}
				</div>
			</div>
	);
}

export default ProductsList;