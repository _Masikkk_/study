import React from 'react';
import ProductsList from '../../components/ProductsList/ProductsList';

const HomePage = ({ products, displayModal, favorites, toggleFavorites }) => {
	return (
		<ProductsList products={products}
			displayModal={displayModal}
			favorites={favorites}
			toggleFavorites={toggleFavorites} />
	);
};

export default HomePage;