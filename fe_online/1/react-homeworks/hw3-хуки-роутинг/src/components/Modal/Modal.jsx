import PropTypes from 'prop-types';
import './modal.scss';

const Modal = (props) => {
	const { modalSettings, modalType, closeModal, addToCart, removeFromCart} = props

	const handleAddToCartClick = (cartItem) => {
		addToCart(cartItem)
	}

	return (
		<div className='popup'>
			<div className='popup__body' onClick={closeModal}>
				{modalType === 'addToCart' &&
					<div className='popup__content'>
						<button className='popup__close'>X</button>
						<div className='popup__title'>Add {modalSettings.name} to cart?</div>
						<div className='popup__text'>{(
							<>
								<img src={modalSettings.url} alt='knife' />
								<p>{modalSettings.price} USD</p>
							</>
						)}</div>
						<div className='popup__action-wrapper'>
							<button className='popup__action action1'
								onClick={event => { handleAddToCartClick(modalSettings) }}>
								Yes</button>
							<button className='popup__action action1'>No</button>
						</div>
					</div>}
				{modalType === 'removeFromCart' &&
					<div className='popup__content'>
						<button className='popup__close'>X</button>
						<div className='popup__title'>Remove {modalSettings.name} from cart?</div>
						<div className='popup__text'>{(
							<>
								<img src={modalSettings.url} alt='knife' />
								<p style={{ textAlign: 'center' }}>{modalSettings.price} USD</p>
							</>
						)}</div>
						<div className='popup__action-wrapper'>
							<button className='popup__action action2'
								onClick={removeFromCart}>
								Yes</button>
							<button className='popup__action action2'>No</button>
						</div>
					</div>}

			</div>
		</div>
	);
}

Modal.defaultProps = {
	modalSettings: {
		name: 'best phone',
		price: 1,
		url: 'img/Samsung Galaxy A52.jpg',
	}
}

Modal.propTypes = {
	modalSettings: PropTypes.object,
	closeModal: PropTypes.func,
	closeButton: PropTypes.bool,
	addToCart: PropTypes.func,
}

export default Modal;