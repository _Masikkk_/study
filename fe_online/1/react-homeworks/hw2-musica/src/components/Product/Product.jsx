import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './product.scss'

class Product extends Component {
	static propTypes = {
		productInfo: PropTypes.object,
		displayModal: PropTypes.func,
		addToFavorite: PropTypes.func,
		favorites: PropTypes.array,
	}

	render() {
		const { productInfo, addToFavorite, favorites, displayModal } = this.props

		return (
			<div className='product'>
				<h2 className='product__name' style={{ color: productInfo.color }}>{productInfo.name}</h2>
				<div className='product__img'>
					<img src={productInfo.url} alt='knife' />
				</div>
				<div className='product__price'>Price: {productInfo.price} USD</div>
				<div className='product__action'>
					{favorites.includes(productInfo.SKU) ?
						<img data-id={productInfo.SKU} onClick={addToFavorite} className='product__favorite' src='img/star-added.png' alt='yellow star' /> :
						<img data-id={productInfo.SKU} onClick={addToFavorite} className='product__favorite' src='img/favorite-2765.svg' alt='black star' />
					}
					<button data-id={productInfo.SKU} onClick={displayModal} className='product__cart'>Add to cart</button>
				</div>
			</div>
		);
	}
}

Product.defaultProps = {
	productInfo: {
		name: 'best knife',
		color: 'orange',
		price: 1,
		url: 'img/Cutter.jpg'
	}
}

export default Product;