import React, { Component } from 'react';
import './button.scss'

class Button extends Component {
	render() {
		return (
			<button
				data-id={this.props.dataId}
				className='btn-modal'
				style={{ backgroundColor: this.props.backgroundColor }}
				onClick={this.props.onClick}>
				{this.props.text}
			</button>
		);
	}
}

export default Button;