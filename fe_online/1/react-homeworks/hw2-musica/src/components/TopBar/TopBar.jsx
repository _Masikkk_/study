import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './topBar.scss'

class TopBar extends Component {
	static propTypes = {
		favorites: PropTypes.array,
		cart: PropTypes.array,
	}

	render() {
		const { favorites = [], cart = [] } = this.props

		return (
			<div className='top-bar'>
				<div className='favorites'>
					<img src='img/star-added.png' alt='star' />
					<span className='favorites__counter'>{favorites.length}</span>
				</div>
				<div className='cart'>
					<img src='img/shopping_cart.svg' alt='star' />
					<span className='cart__counter'>{cart.length}</span>
				</div>
			</div>
		);
	}
}

export default TopBar;