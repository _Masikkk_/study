import React, { Component } from 'react';
import './app.scss'

import Modal from './components/Modal/Modal';
import Header from './components/Header/Header';
import getProducts from './api/getProducts';
import ProductsList from './components/ProductsList/ProductsList';
import TopBar from './components/TopBar/TopBar';

class App extends Component {
	state = {
		noModal: true,
		products: [],
		modalSettings: {},
		favorites: [],
		cart: [],
	}

	componentDidMount() {
		getProducts()
			.then(products => {
				this.setState({
					products: products,
				})
			})

		if (!localStorage.getItem('cart')) {
			localStorage.setItem('cart', JSON.stringify(this.state.cart))
		} else {
			this.setState({
				cart: JSON.parse(localStorage.getItem('cart'))
			})
		}

		if (!localStorage.getItem('favorites')) {
			localStorage.setItem('favorites', JSON.stringify(this.state.favorites))
		} else {
			this.setState({
				favorites: JSON.parse(localStorage.getItem('favorites'))
			})
		}
	}

	displayModal = (event) => {
		const buttonId = event.target.dataset.id
		const modalToDisplay = this.state.products.find(el => el.SKU === buttonId)

		this.setState({
			noModal: !this.state.noModal,
			modalSettings: {
				...modalToDisplay
			}
		})
	}

	closeModal = (event) => {
		event.stopPropagation()

		if (event.target.classList.contains('popup__close') || event.target.classList.contains('popup__body') || event.target.classList.contains('popup__action')) {
			this.setState({
				noModal: !this.state.noModal
			})
		}
	}

	addToCart = (event) => {
		const cartSKU = event.target.dataset.id
		const allSKU = [...this.state.cart, cartSKU]
		this.setState({
			cart: allSKU
		})

		localStorage.setItem('cart', JSON.stringify(allSKU))
	}

	addToFavorite = (event) => {
		const favoritesSKU = event.target.dataset.id
		const allSKU = [...this.state.favorites, favoritesSKU]
		const filteredSKU = this.state.favorites.filter(el => el !== favoritesSKU)

		if (this.state.favorites.includes(favoritesSKU)) {
			this.setState({
				favorites: filteredSKU
			})
			localStorage.setItem('favorites', JSON.stringify(filteredSKU))
		} else {
			this.setState({
				favorites: allSKU
			})
			localStorage.setItem('favorites', JSON.stringify(allSKU))
		}
	}

	render() {
		const { noModal, products, favorites } = this.state

		return (
			<>
				<Header />
				<TopBar favorites={this.state.favorites} cart={this.state.cart} />
				<ProductsList
					products={products}
					displayModal={this.displayModal}
					favorites={favorites}
					addToFavorite={this.addToFavorite}
				/>
				{noModal ? null : <Modal
					closeButton={true}
					modalSettings={this.state.modalSettings}
					closeModal={this.closeModal}
					addToCart={this.addToCart}
				/>}
			</>
		);
	}
}

export default App;