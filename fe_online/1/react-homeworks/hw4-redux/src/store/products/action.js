import getProducts from './../../api/getProducts';
import types from '../types';

const getProductsSuccess = (products) => {
	return {
		type: types.getProductsSuccess,
		payload: {
			products: products
		}
	}
}

const getProductsRequested = (message) => {
	return {
		type: types.getProductsRequested,
		payload: {
			message
		}
	}
}

const getProductsError = (message) => {
	return {
		type: types.getProductsError,
		payload: {
			message
		}
	}
}


export const fetchProducts = () => (dispatch) => {
	dispatch(getProductsRequested('Loading...'))
	getProducts()
		.then(allProducts => dispatch(getProductsSuccess(allProducts)))
		.catch(() => dispatch(getProductsError('Sorry, an error occurred')))
}