import types from "../types";

const setCart = (cartItems) => {
	return {
		type: types.setCart,
		payload: {
			cart: cartItems
		}
	}
}

export default setCart