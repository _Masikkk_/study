import './header.scss';

const Header = () => {
	return (
		<div className='header'>
			<div className='header__text'>Buy</div>
			<img className='header__logo' src='img/header-logo.svg' alt='black knife' />
			<div className='header__text'>Knifes</div>
		</div>
	);
}

export default Header;