import * as yup from 'yup'

export const formSchema = yup.object().shape({
	name: yup
		.string()
		.test('is number', 'Invalid ${path}', value => Number.isNaN(+value))
		.required('This field is required'),
	surname: yup
		.string()
		.test('is number', 'Invalid ${path}', value => Number.isNaN(+value))
		.required('This field is required'),
	age: yup
		.number()
		.integer()
		.positive()
		.max(100)
		.required('This field is required'),
	address: yup
		.string()
		.test('is number', 'Invalid ${path}', value => Number.isNaN(+value))
		.required('This field is required'),
	phone: yup
		.number()
		.typeError('phone must be a number')
		.required('This field is required'),
})