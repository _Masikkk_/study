import express from "express";
import fs from 'fs';

const app = express();

app.get('/', (req, res, next) => {
    fs.readFile('./products1.json', {encoding: "UTF-8"}, (error, data) => {
        if (error) {
            return next(error);
        }
        return res.json(JSON.parse(data));
    })
})

app.post('/', express.json(), (req, res, next) => {
    const product = {
        id: Date.now(),
        name: req.body.name,
        price: req.body.price,
        image: req.body.image,
        vendor: req.body.vendor,
        color: req.body.color,
    };
    fs.readFile('./products.json', {encoding: "UTF-8"}, (error, data) => {
        if (error) {
            return next(error);
        }
        const products = JSON.parse(data);
        const productsNew = [...products, product];
        fs.writeFile('./products.json', JSON.stringify(productsNew), {encoding: "UTF-8"}, (error) => {
            if (error) {
                return next(error);
            }
            return res.status(201).end();
        })
    })
    res.send('Post product')
})

app.put('/:id', express.json(), (req, res, next) => {
    const {id} = req.params;
    const product = {
        id: +id,
        name: req.body.name,
        price: req.body.price,
        image: req.body.image,
        vendor: req.body.vendor,
        color: req.body.color,
    };
    fs.readFile('./products.json', {encoding: "UTF-8"}, (error, data) => {
        if (error) {
            return next(error)
        }
        const products = JSON.parse(data);
        const index = products.findIndex(value => value.id === +id);
        const productsNew = [...products.slice(0, index), product, ...products.slice(index + 1)];

        fs.writeFile('./products.json', JSON.stringify(productsNew), {encoding: "UTF-8"}, (error) => {
            if (error) {
                return next(error);
            }
            return res.end();
        })
    })
})

app.delete('/:id', (req, res, next) => {
    const {id} = req.params;
    fs.readFile('./products.json', {encoding: "UTF-8"}, (error, data) => {
        if (error) {
            return next(error)
        }
        const products = JSON.parse(data);
        const productsNew = products.filter(value => value.id !== +id);
        console.log(productsNew);
        fs.writeFile('./products.json', JSON.stringify(productsNew), {encoding: "UTF-8"}, (error) => {
            if (error) {
                return next(error);
            }
            return res.end();
        })
    })
})

app.use((req, res, next)=>{
    res.status(404).send("Not Found")
}
app.use((err, req, res, next)=>{
    res.status(err.status || 500).end(err.message || "Internal server error");
})
app.listen({host: "localhost", port: 8085}, () => {
    console.log("Express server listening on port 8085!");
});